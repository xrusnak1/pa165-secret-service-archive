# PA165 Secret Service Archive

## How to use:
* you should have `node.js` and `maven` installed
1. compile: `mvn clean install`  
2. run the web application: `cd rest && mvn spring-boot:run`
3. open new terminal window
4. run `cd frontend && yarn install`, you should only run the first time you are starting the app (notice node_modules folder is created)  
5. run `yarn start`
6. to stop the app press `ctrl^C`

* to access the application open url shown in the startup log (usually http://localhost:3000/)  
* to access the endpoints run `mvn spring-boot:run -Dspring-boot.run.profiles=devel` instead and open url shown in application startup logs (usually SWAGGER UI: http://localhost:8080/pa165/rest/swagger-ui/)


## Description
The web application is an archive of missions performed by agents of a secret service. It contains data about countries, missions, agents and reports from each agent from each mission. The data about countries are similar as the CIA World Factbook (google it up). Each mission targets a country, has some duration, objectives and needs some material resources (explosives, submarines etc.). Agents have personal data about their codenames, training, language skills, weapon skills etc. Agents are assigned to a mission for independent periods of time, thus the number of agents assigned to mission may change during its duration. A report about each assignment and an agent’s performance evaluation is archived. The application also helps to find available agents that have previous experience with a country and have required skills for a planned mission.

![class diagram](diagrams/ERD.jpg "Class diagram")
![class diagram](diagrams/UCD.png "Use case diagram")


