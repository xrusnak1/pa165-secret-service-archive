package cz.muni.fi.pa165.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author David Myska
 */
public class AgentDto {
    private Long id;

    private String codename;

    private LoginCredentialsDto loginCredentials;

    private Set<String> languageSkills = new HashSet<>();

    private Set<String> skills = new HashSet<>();

    @JsonIgnore
    private Set<AssignmentDto> assignments = new HashSet<>();

    private LocalDate activeSince;

    private LocalDate dutyEnded = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public LoginCredentialsDto getLoginCredentials() {
        return loginCredentials;
    }

    public void setLoginCredentials(LoginCredentialsDto loginCredentials) {
        this.loginCredentials = loginCredentials;
    }

    public Set<AssignmentDto> getAssignments() {
        return assignments;
    }

    public void setAssignments(Set<AssignmentDto> assignments) {
        this.assignments = assignments;
    }

    public Set<String> getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Set<String> languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Set<String> getSkills() {
        return skills;
    }

    public void setSkills(Set<String> skills) {
        this.skills = skills;
    }

    public LocalDate getActiveSince() {
        return activeSince;
    }

    public void setActiveSince(LocalDate activeSince) {
        this.activeSince = activeSince;
    }

    public LocalDate getDutyEnded() {
        return dutyEnded;
    }

    public void setDutyEnded(LocalDate dutyEnded) {
        this.dutyEnded = dutyEnded;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "id=" + id +
                ", codename='" + codename + '\'' +
                ", languageSkills=" + languageSkills +
                ", skills=" + skills +
                ", activeSince=" + activeSince +
                ", dutyEnded=" + dutyEnded +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AgentDto)) return false;
        AgentDto agent = (AgentDto) o;
        return getCodename().equals(agent.getCodename()) && getLanguageSkills().equals(agent.getLanguageSkills())
                && getSkills().equals(agent.getSkills()) && getActiveSince().equals(agent.getActiveSince())
                && Objects.equals(getDutyEnded(), agent.getDutyEnded());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodename(), getLanguageSkills(), getSkills(), getActiveSince(), getDutyEnded());
    }
}
