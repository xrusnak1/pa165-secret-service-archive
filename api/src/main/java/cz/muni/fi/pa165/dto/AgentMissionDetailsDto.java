package cz.muni.fi.pa165.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
public class AgentMissionDetailsDto {

    private Long id;

    private String codename;

    private Set<String> languageSkills = new HashSet<>();

    private Set<String> skills = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public Set<String> getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Set<String> languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Set<String> getSkills() {
        return skills;
    }

    public void setSkills(Set<String> skills) {
        this.skills = skills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AgentMissionDetailsDto)) return false;
        AgentMissionDetailsDto that = (AgentMissionDetailsDto) o;
        return getCodename().equals(that.getCodename()) && getLanguageSkills().equals(that.getLanguageSkills()) && getSkills().equals(that.getSkills());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodename(), getLanguageSkills(), getSkills());
    }
}
