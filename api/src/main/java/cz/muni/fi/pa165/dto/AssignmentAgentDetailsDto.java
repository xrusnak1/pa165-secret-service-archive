package cz.muni.fi.pa165.dto;

import java.time.LocalDate;
import java.util.Objects;

public class AssignmentAgentDetailsDto {

    private Long id;

    private MissionAgentDetailsDto mission;

    private LocalDate start;

    private LocalDate finish;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MissionAgentDetailsDto getMission() {
        return mission;
    }

    public void setMission(MissionAgentDetailsDto mission) {
        this.mission = mission;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignmentAgentDetailsDto)) return false;
        AssignmentAgentDetailsDto that = (AssignmentAgentDetailsDto) o;
        return getMission().equals(that.getMission()) && getStart().equals(that.getStart()) && getFinish().equals(that.getFinish());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMission(), getStart(), getFinish());
    }
}
