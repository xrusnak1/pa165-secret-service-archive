package cz.muni.fi.pa165.dto;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author David Myska
 */

public class AssignmentCreateDto {
    private Long id;

    private Long agentId;

    private Long missionId;

    private LocalDate start;

    private LocalDate finish;

    private String agentReport;

    private String operatorReport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate end) {
        this.finish = end;
    }

    public String getAgentReport() {
        return agentReport;
    }

    public void setAgentReport(String agentReport) {
        this.agentReport = agentReport;
    }

    public String getOperatorReport() {
        return operatorReport;
    }

    public void setOperatorReport(String operatorReport) {
        this.operatorReport = operatorReport;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + id +
                ", start=" + start +
                ", end=" + finish +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignmentCreateDto)) return false;
        AssignmentCreateDto that = (AssignmentCreateDto) o;
        return getAgentId().equals(that.getAgentId())
                && getMissionId().equals(that.getMissionId())
                && getStart().equals(that.getStart())
                && getFinish().equals(that.getFinish())
                && getAgentReport().equals(that.getAgentReport())
                && getOperatorReport().equals(that.getOperatorReport());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAgentId(), getMissionId(), getStart(), getFinish(), getAgentReport(), getOperatorReport());
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getMissionId() {
        return missionId;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }
}
