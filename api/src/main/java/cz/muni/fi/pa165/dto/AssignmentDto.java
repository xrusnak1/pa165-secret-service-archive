package cz.muni.fi.pa165.dto;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Andrea Fickova
 */

public class AssignmentDto {
    private Long id;

    private AgentDto agent;

    private MissionDto mission;

    private LocalDate start;

    private LocalDate finish;

    private String agentReport = "";

    private String operatorReport = "";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AgentDto getAgent() {
        return agent;
    }

    public void setAgent(AgentDto agent) {
        this.agent = agent;
    }

    public MissionDto getMission() {
        return mission;
    }

    public void setMission(MissionDto mission) {
        this.mission = mission;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate end) {
        this.finish = end;
    }

    public String getAgentReport() {
        return agentReport;
    }

    public void setAgentReport(String agentReport) {
        this.agentReport = agentReport;
    }

    public String getOperatorReport() {
        return operatorReport;
    }

    public void setOperatorReport(String operatorReport) {
        this.operatorReport = operatorReport;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + id +
                ", agent=" + agent.getCodename() +
                ", mission=" + mission.getName() +
                ", start=" + start +
                ", end=" + finish +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignmentDto)) return false;
        AssignmentDto that = (AssignmentDto) o;
        return getAgent().equals(that.getAgent()) && getMission().equals(that.getMission())
                && getStart().equals(that.getStart()) && getFinish().equals(that.getFinish());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAgent(), getMission(), getStart(), getFinish());
    }
}
