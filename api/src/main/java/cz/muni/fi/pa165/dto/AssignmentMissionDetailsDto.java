package cz.muni.fi.pa165.dto;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Jakub Hanko
 */
public class AssignmentMissionDetailsDto {

    private Long id;

    private AgentMissionDetailsDto agent;

    private LocalDate start;

    private LocalDate finish;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AgentMissionDetailsDto getAgent() {
        return agent;
    }

    public void setAgent(AgentMissionDetailsDto agentMissionDetailsDto) {
        this.agent = agentMissionDetailsDto;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignmentMissionDetailsDto)) return false;
        AssignmentMissionDetailsDto that = (AssignmentMissionDetailsDto) o;
        return getAgent().equals(that.getAgent()) && getStart().equals(that.getStart()) && getFinish().equals(that.getFinish());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAgent(), getStart(), getFinish());
    }
}
