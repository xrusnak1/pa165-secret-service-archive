package cz.muni.fi.pa165.dto;

import java.util.Objects;

/**
 * @author David Myska
 */
public class AuthenticationDto {
    private Long id;

    private LoginCredentialsDto loginCredentials;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoginCredentialsDto getLoginCredentials() {
        return loginCredentials;
    }

    public void setLoginCredentials(LoginCredentialsDto loginCredentials) {
        this.loginCredentials = loginCredentials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticationDto)) return false;
        AuthenticationDto that = (AuthenticationDto) o;
        return getLoginCredentials().equals(that.getLoginCredentials());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLoginCredentials());
    }
}
