package cz.muni.fi.pa165.dto;

import java.util.Set;

/**
 * @author David Rusnak
 */
public class CountryDto {

    private Long id;

    private String name;

    private Set<String> languages;

    private Float literacy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public Float getLiteracy() {
        return literacy;
    }

    public void setLiteracy(Float literacy) {
        this.literacy = literacy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CountryDto)) return false;

        CountryDto that = (CountryDto) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        if (getLanguages() != null ? !getLanguages().equals(that.getLanguages()) : that.getLanguages() != null)
            return false;
        return getLiteracy() != null ? getLiteracy().equals(that.getLiteracy()) : that.getLiteracy() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getLanguages() != null ? getLanguages().hashCode() : 0);
        result = 31 * result + (getLiteracy() != null ? getLiteracy().hashCode() : 0);
        return result;
    }
}
