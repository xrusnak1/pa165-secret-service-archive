package cz.muni.fi.pa165.dto;

import java.util.Collection;
import java.util.Objects;

public class MissionAgentDetailsDto {
    private Long id;

    private String name;

    private Collection<String> objectives;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<String> getObjectives() {
        return objectives;
    }

    public void setObjectives(Collection<String> objectives) {
        this.objectives = objectives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MissionAgentDetailsDto)) return false;
        MissionAgentDetailsDto that = (MissionAgentDetailsDto) o;
        return getName().equals(that.getName()) && getObjectives().equals(that.getObjectives());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getObjectives());
    }
}
