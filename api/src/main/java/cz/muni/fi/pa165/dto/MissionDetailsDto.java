package cz.muni.fi.pa165.dto;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
public class MissionDetailsDto {

    private Long id;

    private String name;

    private CountryDto country;

    private LocalDate start;

    private LocalDate deadline;

    private Set<AssignmentMissionDetailsDto> assignments = new HashSet<>();

    private Collection<String> resources;

    private Collection<String> objectives;

    private Collection<String> requiredSkills;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryDto getCountry() {
        return country;
    }

    public void setCountry(CountryDto country) {
        this.country = country;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Set<AssignmentMissionDetailsDto> getAssignments() {
        return this.assignments;
    }

    public void setAssignments(Set<AssignmentMissionDetailsDto> assignments) {
        this.assignments = assignments;
    }

    public Collection<String> getResources() {
        return resources;
    }

    public void setResources(Collection<String> resources) {
        this.resources = resources;
    }

    public Collection<String> getObjectives() {
        return objectives;
    }

    public void setObjectives(Collection<String> objectives) {
        this.objectives = objectives;
    }

    public Collection<String> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(Collection<String> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    @Override
    public String toString() {
        return "MissionDetailsDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country=" + country +
                ", start=" + start +
                ", deadline=" + deadline +
                ", assignments=" + assignments +
                ", resources=" + resources +
                ", objectives=" + objectives +
                ", requiredSkills=" + requiredSkills +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MissionDetailsDto)) return false;
        MissionDetailsDto that = (MissionDetailsDto) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getCountry(), that.getCountry()) && Objects.equals(getStart(), that.getStart()) && Objects.equals(getDeadline(), that.getDeadline());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountry(), getStart(), getDeadline());
    }
}
