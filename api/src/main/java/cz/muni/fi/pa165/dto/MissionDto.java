package cz.muni.fi.pa165.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
public class MissionDto {

    private Long id;

    private String name;

    private CountryDto country;

    private LocalDate start;

    private LocalDate deadline;

    @JsonIgnore
    private Set<AssignmentDto> assignments = new HashSet<>();

    private Collection<String> resources;

    private Collection<String> objectives;

    private Collection<String> requiredSkills;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryDto getCountry() {
        return country;
    }

    public void setCountry(CountryDto country) {
        this.country = country;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Set<AssignmentDto> getAssignments() {
        return this.assignments;
    }

    public void setAssignments(Set<AssignmentDto> assignments) {
        this.assignments = assignments;
    }

    public Collection<String> getResources() {
        return resources;
    }

    public void setResources(Collection<String> resources) {
        this.resources = resources;
    }

    public Collection<String> getObjectives() {
        return objectives;
    }

    public void setObjectives(Collection<String> objectives) {
        this.objectives = objectives;
    }

    public Collection<String> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(Collection<String> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    @Override
    public String toString() {
        return "MissionDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country=" + country +
                ", start=" + start +
                ", deadline=" + deadline +
                ", assignments=" + assignments +
                ", resources=" + resources +
                ", objectives=" + objectives +
                ", requiredSkills=" + requiredSkills +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MissionDto)) return false;
        MissionDto mission = (MissionDto) o;
        return getName().equals(mission.getName()) && getCountry().equals(mission.getCountry())
                && getStart().equals(mission.getStart()) && getDeadline().equals(mission.getDeadline());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountry(), getStart(), getDeadline());
    }
}
