package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.MissionDto;

import java.util.Collection;
import java.util.List;

/**
 * @author David Myska
 */
public interface AgentFacade extends BaseCrudFacade<AgentDto> {
    /**
     * @param agentId id of the agent for which we want to find the suitable missions
     * @return List of all the suitable missions
     */
    List<MissionDto> findMissionsSuitableForAgent(Long agentId);

    Collection<AgentDto> findAll();

    AgentDto findByName(String name);

    <Dto> Dto findByName(String name, Class<Dto> outputType);
}
