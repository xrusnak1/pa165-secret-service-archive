package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AssignmentCreateDto;
import cz.muni.fi.pa165.dto.AssignmentDto;

import java.util.Collection;

/**
 * @author Andrea Fickova
 */

public interface AssignmentFacade extends BaseCrudFacade<AssignmentDto>{
    Collection<AssignmentDto> findAll();
    void createAssignment(AssignmentCreateDto dto);
}
