package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.LoginCredentialsDto;

/**
 * @author David Myska
 */
public interface AuthenticationFacade extends BaseCrudFacade<LoginCredentialsDto> {
    LoginCredentialsDto getUserCredentials(String username);
}
