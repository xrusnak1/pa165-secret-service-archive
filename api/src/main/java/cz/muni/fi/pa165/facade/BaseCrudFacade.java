package cz.muni.fi.pa165.facade;

/**
 * @author David Myska
 */
public interface BaseCrudFacade<DtoType> {

    /**
     * Creates entity based on provided Dto
     * @param dto Dto the new entity is based on
     */
    void create(DtoType dto);

    /**
     * Finds entity with specified id and returns its Dto
     * @param id id of the entity you want to find
     * @return Dto of the entity
     */
    DtoType find(Long id);

    /**
     * Updates entity corresponding to the given Dto
     * @param dto Dto of the entity you want to update
     * @return Dto corresponding to the updated state of the entity
     */
    DtoType update(DtoType dto);

    /**
     * Deletes entity with specified id
     * @param id Id of an entity you want to delete
     */
    void delete(Long id);
}
