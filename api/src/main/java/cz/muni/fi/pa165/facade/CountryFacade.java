package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.CountryDto;

import java.util.Collection;

/**
 * @author David Rusnak
 */
public interface CountryFacade extends BaseCrudFacade<CountryDto> {
    Collection<CountryDto> findAll();
}
