package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.MissionDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Jakub Hanko
 */
public interface MissionFacade extends BaseCrudFacade<MissionDto> {

    /**
     * @param missionId id of the mission to form the assignment
     * @param agentId id of the agent to form the assignment
     * @param start date time of the start of the assignment
     * @param end date time of the end of the assignment
     */
    void assignAgent(Long missionId, Long agentId, LocalDate start, LocalDate end);

    /**
     * @param missionId id of the mission for which we should find the suitable agents
     * @return List of all the suitable agents
     */
    List<AgentDto> findAgentsSuitableForMission(Long missionId);

    /**
     * @return List of all the existing missions.
     */
    List<MissionDto> findAll();

    <Dto> Dto find(Long id, Class<Dto> outputType);
}
