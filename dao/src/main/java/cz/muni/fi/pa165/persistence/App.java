package cz.muni.fi.pa165.persistence;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(PersistenceConfig.class);

        appContext.getBean(InitDB.class).init();
    }
}
