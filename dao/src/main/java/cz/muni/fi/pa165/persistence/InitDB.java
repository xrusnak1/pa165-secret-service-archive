package cz.muni.fi.pa165.persistence;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Component
public class InitDB {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public void init() {
    }

}
