package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Agent;

import javax.persistence.NoResultException;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Daniel Pecuch
 * Dao for Agent entity
 */
public interface AgentDao extends GenericBaseDao<Agent> {

    /**
     * Finds an agent by his name
     * @param name of the agent to be found
     * @return the agent entity with the given name or null
     * @throws NoResultException if there is no result
     */
    Agent findByName(String name);

    /**
     * Finds agents available in certain timeslot (i.e. they are not assigned to any mission and they are
     * on active duty)
     * @param start start of the timeslot
     * @param end end of the timeslot
     * @return list of agents who are free to go on a mission in the given timeslot
     */
    List<Agent> findAgentsAvailableInTime(LocalDate start, LocalDate end);
}
