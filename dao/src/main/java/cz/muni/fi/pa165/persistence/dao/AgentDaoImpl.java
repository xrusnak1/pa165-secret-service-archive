package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Agent;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Jakub Hanko
 */
@Repository
public class AgentDaoImpl extends GenericBaseDaoImpl<Agent> implements AgentDao {

    @Override
    public Agent findByName(String name) {
        return em.createQuery("select a from Agent a where a.codename = :name", Agent.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public List<Agent> findAgentsAvailableInTime(LocalDate start, LocalDate end) {
        return em.createQuery("select a from Agent a " +
                                    "where ( " +
                                        "(a.activeSince between :start and :end " +
                                        "and a.dutyEnded is null)" +
                                        " or (:start between a.activeSince and a.dutyEnded " +
                                                "and :end between a.activeSince and a.dutyEnded) " +
                                        ")" +
                                        "and not exists " +
                                        "( " +
                                            "select id from Assignment asg " +
                                            "where asg.agent.id = a.id " +
                                                "and asg.start <= :end " +
                                                "and asg.finish >= :start" +
                                        ")", Agent.class)
                .setParameter("start", start)
                .setParameter("end", end)
                .getResultList();
    }
}
