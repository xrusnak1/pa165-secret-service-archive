package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Assignment;

/**
 * @author David Rusnak
 * Dao for Assignment entity
 */
public interface AssignmentDao extends GenericBaseDao<Assignment> {
}
