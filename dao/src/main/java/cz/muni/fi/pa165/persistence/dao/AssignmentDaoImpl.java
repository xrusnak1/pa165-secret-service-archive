package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Assignment;
import org.springframework.stereotype.Repository;

/**
 * @author Andrea Fickova
 */
@Repository
public class AssignmentDaoImpl extends GenericBaseDaoImpl<Assignment> implements AssignmentDao {
}
