package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Country;

/**
 * @author David Rusnak
 */
public interface CountryDao extends GenericBaseDao<Country> {
}
