package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Country;
import org.springframework.stereotype.Repository;

/**
 * @author David Rusnak
 */
@Repository
public class CountryDaoImpl extends GenericBaseDaoImpl<Country> implements CountryDao {
}
