package cz.muni.fi.pa165.persistence.dao;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * @author David Rusnak
 */
public interface GenericBaseDao<T> {
    void create(T e);

    /**
     * Finds entity with given id
     * @param id of the entity to be found
     * @return found entity
     * @throws DataAccessException if instance is not an entity or is a removed entity
     */
    T find(Long id);

    /**
     * Finds and returns all entities
     * @return all entities
     */
    List<T> findAll();

    /**
     * Updates given entity
     * @param e entity to be updated
     * @return the managed instance that the state was merged to
     * @throws DataAccessException if instance is not an entity or is a removed entity
     */
    T update(T e);

    /**
     * Deletes entity with given id
     * @param id of entity to be deleted
     * @throws DataAccessException - if the instance is not an entity or is a detached entity
     */
    void delete(Long id);
}
