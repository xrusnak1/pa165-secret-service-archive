package cz.muni.fi.pa165.persistence.dao;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author David Rusnak
 */
@NoRepositoryBean
@Transactional
public abstract class GenericBaseDaoImpl<T> implements GenericBaseDao<T> {

    @PersistenceContext
    EntityManager em;

    private Class<T> type;

    public GenericBaseDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public void create(T e) {
        em.persist(e);
    }

    @Override
    public T find(Long id) {
        return em.find(type, id);
    }

    @Override
    public T update(T e) {
        return em.merge(e);
    }

    @Override
    public void delete(Long id) {
        em.remove(em.getReference(type, id));
    }

    @Override
    public List<T> findAll() {
        return em.createQuery("from " + type.getName(), type).getResultList();
    }
}
