package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.LoginCredentials;

/**
 * @author David Myska
 */
public interface LoginCredentialsDao extends GenericBaseDao<LoginCredentials> {

    LoginCredentials findByUsername(String username);

}
