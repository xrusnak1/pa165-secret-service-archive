package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.LoginCredentials;
import org.springframework.stereotype.Repository;

/**
 * @author David Myska
 */
@Repository
public class LoginCredentialsDaoImpl extends GenericBaseDaoImpl<LoginCredentials> implements LoginCredentialsDao {

    @Override
    public LoginCredentials findByUsername(String username) {
        return em.createQuery("select lc from LoginCredentials lc where lc.username = :username", LoginCredentials.class)
                .setParameter("username", username)
                .getSingleResult();
    }
}
