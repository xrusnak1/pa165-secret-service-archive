package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Mission;

import java.time.LocalDate;
import java.util.List;

/**
 * Andrea Fickova
 */

public interface MissionDao extends GenericBaseDao<Mission> {
    List<Mission> getFutureMissionsEndingBefore(LocalDate time);
}