package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Andrea Fickova
 */
@Repository
public class MissionDaoImpl extends GenericBaseDaoImpl<Mission> implements MissionDao {
    @Override
    public List<Mission> getFutureMissionsEndingBefore(LocalDate time) {
        return em.createQuery("select m from Mission m where :now < m.start and m.deadline < :time", Mission.class)
                .setParameter("now", LocalDate.now())
                .setParameter("time", time)
                .getResultList();
    }
}