package cz.muni.fi.pa165.persistence.entity;

import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author David Myska
 */
@Entity
@Check(constraints = "activeSince <= dutyEnded")
public class Agent {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, unique = true)
    private String codename;

    @OneToOne
    private LoginCredentials loginCredentials;

    @ElementCollection
    private Set<String> languageSkills = new HashSet<>();

    @ElementCollection
    private Set<String> skills = new HashSet<>();

    @OneToMany(mappedBy = "agent")
    private Set<Assignment> assignments = new HashSet<>();

    @Column(nullable = false)
    private LocalDate activeSince;

    private LocalDate dutyEnded = null;

    public Agent() {
    }

    public Agent(String codename, LocalDate activeSince) {
        this.codename = codename;
        this.activeSince = activeSince;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public LoginCredentials getLoginCredentials() {
        return loginCredentials;
    }

    public void setLoginCredentials(LoginCredentials loginCredentials) {
        this.loginCredentials = loginCredentials;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Set<String> getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(Set<String> languageSkills) {
        this.languageSkills = languageSkills;
    }

    public Set<String> getSkills() {
        return skills;
    }

    public void setSkills(Set<String> skills) {
        this.skills = skills;
    }

    public LocalDate getActiveSince() {
        return activeSince;
    }

    public void setActiveSince(LocalDate activeSince) {
        this.activeSince = activeSince;
    }

    public LocalDate getDutyEnded() {
        return dutyEnded;
    }

    public void setDutyEnded(LocalDate dutyEnded) {
        this.dutyEnded = dutyEnded;
    }

    public void addAssignment(Assignment assignment) {
        assignments.add(assignment);
        assignment.setAgent(this);
    }

    @Override
    public String toString() {
        return "Agent{" +
                "id=" + id +
                ", codename='" + codename + '\'' +
                ", languageSkills=" + languageSkills +
                ", skills=" + skills +
                ", activeSince=" + activeSince +
                ", dutyEnded=" + dutyEnded +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Agent)) return false;
        Agent agent = (Agent) o;
        return getCodename().equals(agent.getCodename()) && getLanguageSkills().equals(agent.getLanguageSkills())
                && getSkills().equals(agent.getSkills()) && getActiveSince().equals(agent.getActiveSince())
                && Objects.equals(getDutyEnded(), agent.getDutyEnded());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodename(), getLanguageSkills(), getSkills(), getActiveSince(), getDutyEnded());
    }
}
