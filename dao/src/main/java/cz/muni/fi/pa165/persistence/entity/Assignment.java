package cz.muni.fi.pa165.persistence.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author David Myska
 */
@Entity
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    private Agent agent;

    @ManyToOne
    private Mission mission;

    @Column(nullable = false)
    private LocalDate start;

    @Column(nullable = false)
    private LocalDate finish;

    @Column
    private String agentReport = "";

    @Column
    private String operatorReport = "";

    public Assignment() {
    }

    public Assignment(Agent agent, Mission mission, LocalDate start, LocalDate finish) {
        this.agent = agent;
        this.mission = mission;
        this.start = start;
        this.finish = finish;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate end) {
        this.finish = end;
    }

    public String getAgentReport() {
        return agentReport;
    }

    public void setAgentReport(String agentReport) {
        this.agentReport = agentReport;
    }

    public String getOperatorReport() {
        return operatorReport;
    }

    public void setOperatorReport(String operatorReport) {
        this.operatorReport = operatorReport;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + id +
                ", agent=" + agent.getCodename() +
                ", mission=" + mission.getName() +
                ", start=" + start +
                ", end=" + finish +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Assignment)) return false;
        Assignment that = (Assignment) o;
        return getAgent().equals(that.getAgent()) && getMission().equals(that.getMission())
                && getStart().equals(that.getStart()) && getFinish().equals(that.getFinish());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAgent(), getMission(), getStart(), getFinish());
    }
}
