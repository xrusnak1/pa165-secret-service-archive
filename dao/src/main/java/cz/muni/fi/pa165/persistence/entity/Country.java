package cz.muni.fi.pa165.persistence.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * @author David Rusnak
 */
@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ElementCollection
    private Set<String> languages;

    private Float literacy;

    public Country() {
    }

    public Country(String name, Set<String> languages, Float literacy) {
        this.name = name;
        this.languages = languages;
        this.literacy = literacy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public Float getLiteracy() {
        return literacy;
    }

    public void setLiteracy(Float literacy) {
        this.literacy = literacy;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", languages=" + languages +
                ", literacy=" + literacy +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(getName(), country.getName()) &&
                Objects.equals(getLanguages(), country.getLanguages()) &&
                Objects.equals(getLiteracy(), country.getLiteracy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLanguages(), getLiteracy());
    }
}
