package cz.muni.fi.pa165.persistence.entity;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class LoginCredentials {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    private String username;

    private String password;
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginCredentials)) return false;
        LoginCredentials that = (LoginCredentials) o;
        return Objects.equals(getUsername(), that.getUsername()) && Objects.equals(getPassword(),
                that.getPassword()) && Objects.equals(getRole(), that.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getPassword(), getRole());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String codename) {
        this.username = codename;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
