package cz.muni.fi.pa165.persistence.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
@Entity
public class Mission {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToOne
    private Country country;

    @Column(nullable = false)
    private LocalDate start;

    @Column(nullable = false)
    private LocalDate deadline;

    @OneToMany(mappedBy = "mission")
    private Set<Assignment> assignments = new HashSet<>();

    @ElementCollection
    private Collection<String> resources;

    @ElementCollection
    private Collection<String> requiredSkills;

    @ElementCollection
    private Collection<String> objectives;

    public Mission() {}

    public Mission(Long id, String name, Country country, LocalDate start, LocalDate deadline) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.start = start;
        this.deadline = deadline;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Set<Assignment> getAssignments() {
        return this.assignments;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Collection<String> getResources() {
        return resources;
    }

    public void setResources(Collection<String> resources) {
        this.resources = resources;
    }

    public Collection<String> getObjectives() {
        return objectives;
    }

    public void setObjectives(Collection<String> objectives) {
        this.objectives = objectives;
    }

    @Override
    public String toString() {
        return "Mission{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country=" + country +
                ", start=" + start +
                ", deadline=" + deadline +
                ", resources=" + resources +
                ", objectives=" + objectives +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mission)) return false;
        Mission mission = (Mission) o;
        return getName().equals(mission.getName()) && getCountry().equals(mission.getCountry())
                && getStart().equals(mission.getStart()) && getDeadline().equals(mission.getDeadline());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountry(), getStart(), getDeadline());
    }

    public Collection<String> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(Collection<String> neededSkills) {
        this.requiredSkills = neededSkills;
    }
}
