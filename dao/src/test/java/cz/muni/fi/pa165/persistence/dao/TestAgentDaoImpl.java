package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.PersistenceConfig;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Daniel Pecuch
 */
@ContextConfiguration(classes = PersistenceConfig.class)
public class TestAgentDaoImpl extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private AgentDao agentDao;

    private Agent agent1 = null;
    private Agent agent2 = null;

    private Mission mission1 = null;

    private Country country1 = null;

    private Assignment assignment1 = null;
    private Assignment assignment2 = null;

    @BeforeClass
    void setUp() {
        runInTransaction(em -> {
            //Creating Agent 1
            agent1 = new Agent("Hawk", LocalDate.of(1990, Month.JANUARY, 1));

            //Creating Agent 2
            agent2 = new Agent("Rock", LocalDate.of(1980, Month.JANUARY, 1));

            country1 = new Country();
            country1.setName("Russia");
            //c1.setLanguages(Set.of(new Locale("ru")));
            country1.setLiteracy(0.87f);

            mission1 = new Mission();
            mission1.setName("Kill Osama");
            mission1.setStart(LocalDate.now());
            mission1.setDeadline(LocalDate.now().plusDays(1));
            mission1.setCountry(country1);

            assignment1 = new Assignment(agent1, mission1, LocalDate.of(1995, 1, 2),
                    LocalDate.of(1996, 1, 2));

            em.persist(assignment1);

            assignment2 = new Assignment(agent2, mission1, LocalDate.of(1999, 1, 2),
                    LocalDate.of(2005, 1, 2));

            em.persist(assignment2);

            em.persist(agent1);
            em.persist(agent2);
            em.persist(mission1);
            em.persist(country1);
        });
    }

    @Test(dependsOnMethods = "testFind")
    public void testCreate() {
        Agent a = new Agent("Parrot", LocalDate.of(1980, Month.JANUARY, 1));
        agentDao.create(a);
        Assert.assertEquals(a, agentDao.find(a.getId()));
    }

    @Test
    public void testCreateWithExistingEntity() {
        Assert.expectThrows(DataAccessException.class, () -> agentDao.create(agent1));
    }

    @Test
    public void testCreateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> agentDao.create(null));
    }

    @Test
    public void testFind() {
        Assert.assertEquals(agentDao.find(agent1.getId()), agent1);
    }

    @Test
    public void testFindWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> agentDao.find(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testUpdate() {
        String newCodeName = "Lion";
        agent1.setCodename(newCodeName);
        agentDao.update(agent1);
        Assert.assertEquals(agentDao.find(agent1.getId()).getCodename(), newCodeName);
    }

    @Test
    public void testUpdateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> agentDao.update(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testDelete() {
        agentDao.delete(agent2.getId());
        Assert.assertNull(agentDao.find(agent2.getId()));
    }

    @Test
    public void testDeleteWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> agentDao.delete(null));
    }

    @Test
    public void testFindAll() {
        List<Agent> result = agentDao.findAll();
        Assert.assertEquals(result.size(), 2);
        Assert.assertEquals(result, List.of(agent1, agent2));
    }

    @Test
    public void testFindByName() {
        Assert.assertEquals(agentDao.findByName("Hawk"), agent1);
    }

    @Test
    public void testFindAvailableAgentsInTime_returnsEmptyList_whenNoActiveAgent() {
        Assert.assertEquals(agentDao.findAgentsAvailableInTime(
                LocalDate.of(1, 1, 1),
                LocalDate.of(1000, 1, 1)).size(), 0);
    }

    @Test
    public void testFindAvailableAgentsInTime_returnsNonEmptyList_whenActiveAgents() {
        Assert.assertEquals(agentDao.findAgentsAvailableInTime(
                LocalDate.of(1980, 1, 1),
                LocalDate.of(1989, 1, 1)).size(), 1);
    }

    @Test
    public void testFindAvailableAgentsInTime_returnsEmptyList_whenActiveAgentsOnAssignmentsOverlap() {
        Assert.assertEquals(agentDao.findAgentsAvailableInTime(
                LocalDate.of(1980, 1, 1),
                LocalDate.of(2000, 1, 1)).size(), 0);
    }

    @Test
    public void testFindAvailableAgentsInTime_returnsNonEmptyList_whenActiveAgentsOnAssignments() {
        Assert.assertEquals(agentDao.findAgentsAvailableInTime(
                LocalDate.of(1980, 1, 1),
                LocalDate.of(1997, 1, 1)).size(), 1);
    }

    @Test(dependsOnMethods = "testUpdate")
    public void testFindAvailableAgentsInTime_returnsEmptyList_whenDutyEnded() {
        agent2.setDutyEnded(LocalDate.of(1989, 1, 2));
        Assert.assertEquals(agentDao.findAgentsAvailableInTime(
                LocalDate.of(1980, 1, 1),
                LocalDate.of(1989, 1, 1)).size(), 1);
    }

    private void runInTransaction(Consumer<EntityManager> consumer) {
        EntityManager em = Persistence.createEntityManagerFactory("default").createEntityManager();

        try {
            em.getTransaction().begin();

            consumer.accept(em);

            em.getTransaction().commit();
        } finally {
            em.close();
        }

    }
}
