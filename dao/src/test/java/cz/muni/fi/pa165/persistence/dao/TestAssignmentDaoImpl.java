package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.PersistenceConfig;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Set;

/**
 * @author David Myska
 */
@ContextConfiguration(classes = PersistenceConfig.class)
public class TestAssignmentDaoImpl extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private AssignmentDao assignmentDao;

    private Agent agent1 = null;
    private Mission mission1 = null;
    private Country country1 = null;
    private Assignment assignment = null;

    @BeforeClass
    void setUp() {
        EntityManager em = Persistence.createEntityManagerFactory("default").createEntityManager();

        try {
            em.getTransaction().begin();

            agent1 = new Agent("Hawk", LocalDate.of(1990, Month.JANUARY, 1));

            country1 = new Country("EN", Set.of("en"), 0.57f);

            em.persist(agent1);
            em.persist(country1);

            mission1 = new Mission(null, "Mission1", country1,
                    LocalDate.of(2000, Month.AUGUST, 10),
                    LocalDate.of(2000, Month.AUGUST, 20));

            em.persist(mission1);

            assignment = new Assignment(agent1, mission1, mission1.getStart(), mission1.getDeadline());

            em.persist(assignment);

            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Test(dependsOnMethods = "testFind")
    public void testCreate() {
        Assignment assignment2 = new Assignment(agent1, mission1, assignment.getStart(), assignment.getStart().plusDays(2));
        assignmentDao.create(assignment2);
        Assert.assertEquals(assignment2, assignmentDao.find(assignment2.getId()));
    }

    @Test
    public void testCreateWithExistingEntity() {
        Assert.expectThrows(DataAccessException.class, () -> assignmentDao.create(assignment));
    }

    @Test
    public void testCreateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> assignmentDao.create(null));
    }

    @Test
    public void testFind() {
        Assert.assertEquals(assignmentDao.find(assignment.getId()), assignment);
    }

    @Test
    public void testFindWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> assignmentDao.find(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testUpdate() {
        LocalDate newEndOfAssignment = assignment.getFinish().minusDays(2);
        assignment.setFinish(newEndOfAssignment);
        assignmentDao.update(assignment);
        Assert.assertEquals(assignmentDao.find(assignment.getId()).getFinish(), newEndOfAssignment);
    }

    @Test
    public void testUpdateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> assignmentDao.update(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testDelete() {
        assignmentDao.delete(assignment.getId());
        Assert.assertNull(assignmentDao.find(assignment.getId()));
    }

    @Test
    public void testDeleteWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> assignmentDao.delete(null));
    }

    @Test
    public void testFindAll() {
        List<Assignment> result = assignmentDao.findAll();
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result, List.of(assignment));
    }
}
