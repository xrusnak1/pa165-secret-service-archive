package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.PersistenceConfig;
import cz.muni.fi.pa165.persistence.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
@ContextConfiguration(classes = PersistenceConfig.class)
public class TestCountryDaoImpl extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private CountryDao countryDao;

    private Country country1 = null;
    private Country country2 = null;

    @BeforeClass
    void setUp() {
        EntityManager em = Persistence.createEntityManagerFactory("default").createEntityManager();

        try {
            em.getTransaction().begin();

            //In the beginning we always have 2 countries
            country1 = new Country("EN", Set.of("en"), 0.57f);
            country2 = new Country("CZ", Set.of("cz"), 0.67f);
            em.persist(country1);
            em.persist(country2);

            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Test(dependsOnMethods = "testFind")
    public void testCreate() {
        Country c = new Country("DE", Set.of("en"), 0.77f);
        countryDao.create(c);

        Assert.assertEquals(c, countryDao.find(c.getId()));
    }

    @Test
    public void testCreateWithExistingEntity() {
        Assert.expectThrows(DataAccessException.class, () -> countryDao.create(country1));
    }

    @Test
    public void testFind() {
        Assert.assertEquals(countryDao.find(country1.getId()).getName(), country1.getName());
    }

    @Test
    public void testFindWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> countryDao.find(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testUpdate() {
        country1.setName("UNKNOWN");
        countryDao.update(country1);

        Assert.assertEquals(countryDao.find(country1.getId()).getName(), "UNKNOWN");
    }

    @Test
    public void testUpdateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> countryDao.update(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testDelete() {
        countryDao.delete(country1.getId());

        Assert.assertNull(countryDao.find(country1.getId()));
    }

    @Test
    public void testDeleteWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> countryDao.delete(null));
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(2, countryDao.findAll().size());
        Assert.assertTrue(countryDao.findAll().containsAll(List.of(country1, country2)));
    }
}
