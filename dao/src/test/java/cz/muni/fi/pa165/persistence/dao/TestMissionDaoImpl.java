package cz.muni.fi.pa165.persistence.dao;

import cz.muni.fi.pa165.persistence.PersistenceConfig;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Set;

/**
 * @author Andrea Fickova
 */

@ContextConfiguration(classes = PersistenceConfig.class)
public class TestMissionDaoImpl extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MissionDao missionDao;

    private Country country1 = null;
    private Country country2 = null;
    private Mission mission1 = null;
    private Mission mission2 = null;

    @BeforeClass
    void setUp() {
        EntityManager em = Persistence.createEntityManagerFactory("default").createEntityManager();

        try {
            // Creating countries for missions
            em.getTransaction().begin();
            country1 = new Country("EN", Set.of("en"), 0.57f);
            country2 = new Country("CH", Set.of("zh"), 0.52f);
            em.persist(country1);
            em.persist(country2);

            // Creating two missions: m1 and m2
            mission1 = new Mission(null,"Save prisoner", country1, LocalDate.of(2020, Month.APRIL, 12), LocalDate.of(2020, Month.APRIL, 23));
            mission2 = new Mission(null,"End corona", country2, LocalDate.of(2020, Month.MARCH, 1), LocalDate.of(2021, Month.JULY, 1));

            em.persist(mission1);
            em.persist(mission2);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Test(dependsOnMethods = "testFind")
    public void testCreate() {
        Mission m3 = new Mission(null, "Protect the president", country2, LocalDate.of(2021, Month.MAY, 1), LocalDate.of(2021, Month.MAY, 3));
        missionDao.create(m3);

        Assert.assertEquals(m3, missionDao.find(m3.getId()));
    }

    @Test
    public void testFind() {
        Assert.assertEquals(missionDao.find(mission1.getId()).getName(), mission1.getName());
    }


    @Test(dependsOnMethods = "testFind")
    public void testDelete() {
        missionDao.delete(mission2.getId());
        Assert.assertNull(missionDao.find(mission2.getId()));
    }

    @Test
    public void testCreateWithExistingEntity() {
        Assert.expectThrows(DataAccessException.class, () -> missionDao.create(mission1));
    }

    @Test
    public void testCreateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> missionDao.create(null));
    }


    @Test
    public void testFindWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> missionDao.find(null));
    }

    @Test(dependsOnMethods = "testFind")
    public void testUpdate() {
        String newMissionName = "Find lost girl";
        mission1.setName(newMissionName);
        missionDao.update(mission1);
        Assert.assertEquals(missionDao.find(mission1.getId()).getName(), newMissionName);
    }

    @Test
    public void testUpdateWithNullEntity() {
        Assert.expectThrows(DataAccessException.class, () -> missionDao.update(null));
    }


    @Test
    public void testDeleteWithNullId() {
        Assert.expectThrows(DataAccessException.class, () -> missionDao.delete(null));
    }

    @Test
    public void testFindAll() {
        List<Mission> result = missionDao.findAll();
        Assert.assertEquals(result.size(), 2);
        Assert.assertEquals(result, List.of(mission1, mission2));
    }

    @Test(dependsOnMethods = "testCreate")
    public void testGetMissionsEndingBefore_EarlyStart() {
        var beforeNow = LocalDate.now().minusDays(3);
        var afterEnd = LocalDate.now().plusDays(6);
        var mStartBefore = new Mission(null,"Before", country2, beforeNow, LocalDate.now().plusDays(4));

        missionDao.create(mStartBefore);

        Assert.assertTrue(missionDao.getFutureMissionsEndingBefore(afterEnd).isEmpty());
    }

    @Test(dependsOnMethods = "testCreate")
    public void testGetMissionsEndingBefore_LateEnd() {
        var afterEnd = LocalDate.now().plusDays(6);
        var mEndAfter = new Mission(null,"After", country2, LocalDate.now().plusDays(3), afterEnd);

        missionDao.create(mEndAfter);

        Assert.assertTrue(missionDao.getFutureMissionsEndingBefore(afterEnd).isEmpty());
    }

    @Test(dependsOnMethods = "testCreate")
    public void testGetMissionsEndingBefore_TwoCorrectMissions() {
        var afterEnd = LocalDate.now().plusDays(6);
        var mCorrect1 = new Mission(null,"Correct1", country2, LocalDate.now().plusDays(1), LocalDate.now().plusDays(4));
        var mCorrect2 = new Mission(null,"Correct2", country2, LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));

        missionDao.create(mCorrect1);
        missionDao.create(mCorrect2);

        Set<Mission> expected = Set.of(mCorrect1, mCorrect2);

        Assert.assertEquals(Set.copyOf(missionDao.getFutureMissionsEndingBefore(afterEnd)), expected);
    }

    @Test(dependsOnMethods = "testCreate")
    public void testGetMissionsEndingBefore_CombinationEarlyCorrectLate() {
        var beforeNow = LocalDate.now().minusDays(3);
        var afterEnd = LocalDate.now().plusDays(6);

        var mStartBefore = new Mission(null,"Before", country2, beforeNow, LocalDate.now().plusDays(4));
        var mEndAfter = new Mission(null,"After", country2, LocalDate.now().plusDays(3), afterEnd);
        var mCorrect1 = new Mission(null,"Correct1", country2, LocalDate.now().plusDays(1), LocalDate.now().plusDays(4));
        var mCorrect2 = new Mission(null,"Correct2", country2, LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));

        missionDao.create(mCorrect1);
        missionDao.create(mCorrect2);
        missionDao.create(mStartBefore);
        missionDao.create(mEndAfter);

        Set<Mission> expected = Set.of(mCorrect1, mCorrect2);

        Assert.assertEquals(Set.copyOf(missionDao.getFutureMissionsEndingBefore(afterEnd)), expected);
    }

}
