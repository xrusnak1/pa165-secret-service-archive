import React, {Component} from "react";
import './App.css';
import AppNavbar from "./components/utilities/AppNavbar";
import MainWindow from "./components/utilities/MainWindow";
import Cookies from "universal-cookie";

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = this.getStateFromCookie()

        this.updateState = this.updateState.bind(this)
        this.getAuthHeader = this.getAuthHeader.bind(this)
        this.hasRole = this.hasRole.bind(this)
    }

    componentDidMount() {
        this.updateState()
    }

    getStateFromCookie() {
        const cookies = new Cookies();
        const jwt = cookies.get("jwt")
        const isLoggedIn = jwt != null
        let token;
        let username;
        let role;
        if (isLoggedIn) {
            token = jwt.token
            username = jwt.username
            role = jwt.role
        }
        return {
            isLoggedIn: isLoggedIn,
            token: token,
            username: username,
            role: role
        }
    }


    updateState() {
        this.setState(this.getStateFromCookie())
    }

    getAuthHeader() {
        return {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + (this.state.token ? this.state.token : "")
        }
    }

    hasRole(role) {
        return role === this.state.role
    }

    render() {
        return (
            <div>
                <AppNavbar user={this.state}/>
                <MainWindow user={this.state} getAuthHeader={this.getAuthHeader} updateHandle={this.updateState}/>
            </div>
        )
    }
}