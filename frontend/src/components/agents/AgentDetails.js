import React, {Component} from "react"
import {Button, ButtonGroup, Container, Table} from "reactstrap";
import Loading from "../utilities/Loading";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import {Link} from "react-router-dom";
import {ViewDetailsButton} from "../utilities/Buttons";

/**
 * @author David Myska
 */
export default class AgentDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            codename: "",
            activeSince: "",
            dutyEnded: "",
            assignments: [],
            languageSkills: [],
            skills: [],
            suitableMissions: [],
            isLoading: true,
            status: 200
        }
        this.delete = this.delete.bind(this)
    }

    async componentDidMount() {
        this.setState({isLoading: true});
        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }

        let fetchId = this.props.match.params.id

        if (fetchId === "profile") {
            await fetch(`/pa165/rest/agent/myid`, headerOptions)
                .then(response => response.json())
                .then(data => fetchId = data)
        }

        fetch(`/pa165/rest/agent/details/` + fetchId, headerOptions)
            .then(response => response.json())
            .then((data) => {this.setState(data); this.setState({isLoading: false})});

        fetch(`/pa165/rest/agent/suitablemissions/` + fetchId, headerOptions)
            .then(response => response.json())
            .then((data) => this.setState({suitableMissions: Array.from(data)}));
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }

        await fetch("/pa165/rest/agent/delete/" + id, headerOptions)
        this.props.history.goBack()
    }

    deleteButton(agentId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                size="sm"
                color="danger"
                onClick={() => this.delete(agentId)}
            > Delete </Button>
        )
    }

    editAssignmentButton(assignmentId) {
        return (
            <Button
                color="primary"
                tag={Link}
                to={"/assignments/" + assignmentId}
            > Edit </Button>
        )
    }

    editAgentButton(agentId) {
        return (
            <Button
                size="sm"
                color="primary"
                tag={Link}
                to={"/agents/" + agentId}
            > Edit </Button>
        )
    }


    render() {
        if (this.state.status >= 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        if (this.state.isLoading) {
            return <Loading/>
        }

        function intersperse(arr, sep) {
            if (arr.length === 0) {
                return [];
            }

            return arr.slice(1).reduce(function (xs, x) {
                return xs.concat([sep, x]);
            }, [arr[0]]);
        }

        const {id, codename, activeSince, dutyEnded, assignments,
            languageSkills, skills, suitableMissions, isLoading} = this.state;

        if (isLoading) {
            return <Loading/>
        }

        let dutyEndedLine = <tr><td>Agent's duty ended</td><td>In active duty</td></tr>;
        if (dutyEnded) {
            dutyEndedLine = <tr><td>Agent's duty ended</td><td>{dutyEnded}</td></tr>
        }
        const knownLanguages = intersperse(languageSkills, ',')
        const acquiredSkills = intersperse(skills, ',')

        const assignmentsLines = assignments.map((assignment) => {
            return (
                <tr key={assignment.id}>
                    <td>{assignment.mission.name}</td>
                    <td>{assignment.start} </td>
                    <td>{assignment.finish}</td>
                    <td>{assignment.mission.objectives}</td>
                    <td>
                        <ButtonGroup>
                            { ViewDetailsButton("/assignments/details/" + assignment.id) }
                            {this.editAssignmentButton(assignment.id)}
                        </ButtonGroup>
                    </td>
                </tr>
            );
        })
        let suitableMissionsTable;
        if (suitableMissions && suitableMissions.length > 0) {
            suitableMissionsTable =
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="10%">Mission name</th>
                            <th width="10%">Country</th>
                            <th width="10%">Mission start</th>
                            <th width="10%">Mission deadline</th>
                            <th width="20%">Required skills</th>
                            <th width="40%">Mission objectives</th>
                        </tr>
                        </thead>
                        <tbody>
                        {suitableMissions.map(mission => {
                            return (
                        <tr key={mission.id}>
                            <td>{mission.name}</td>
                            <td>{mission.country.name}</td>
                            <td>{mission.start}</td>
                            <td>{mission.deadline}</td>
                            <td>{intersperse(mission.requiredSkills, ',')}</td>
                            <td>{intersperse(mission.objectives, ',')}</td>
                        </tr>)
                        })}
                        </tbody>
                    </Table>
        } else {
            suitableMissionsTable = "No suitable missions for this agent."
        }

        return (
            <div>
                <Container fluid>
                    <h2>Agent details</h2>
                    {this.editAgentButton(id)}
                    {this.deleteButton(id)}
                    <h3>Basic info</h3>
                    <Table className="mt-4">
                        <tbody>
                        <tr><td>Codename</td><td>{codename}</td></tr>
                        <tr><td>Is active since</td><td>{activeSince}</td></tr>
                        {dutyEndedLine}
                        <tr><td>Known languages</td><td>{knownLanguages}</td></tr>
                        <tr><td>Acquired skills</td><td>{acquiredSkills}</td></tr>
                        </tbody>
                    </Table>
                    <h3>Suitable missions</h3>
                    {suitableMissionsTable}
                    <h3>Assignments</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="20%">Mission name</th>
                            <th width="15%">Assigned since</th>
                            <th width="15%">Assignment ended</th>
                            <th width="50%">Mission objectives</th>
                        </tr>
                        </thead>
                        <tbody>{assignmentsLines}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}