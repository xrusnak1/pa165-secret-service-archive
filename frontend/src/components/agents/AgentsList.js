import React, {Component} from "react"
import {Button, ButtonGroup, Container, Table} from "reactstrap";
import {Link} from "react-router-dom";
import Loading from "../utilities/Loading";
import BasicErrorHandler from "../utilities/BasicErrorHandler";

/**
 * @author David Myska
 */
export default class AgentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agents: [],
            isLoading: true,
            status: 200
        }
        this.delete = this.delete.bind(this)
    }

    componentDidMount() {
        this.setState({isLoading: true});

        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }
        fetch("/pa165/rest/agent/all", headerOptions)
            .then(response => response.json())
            .then((data) => this.setState({agents: data, status: data.status, isLoading: false}));
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }
        await fetch("/pa165/rest/agent/delete/" + id, headerOptions)
            .then(() => {
                const newAgents = this.state.agents.filter((agent) => agent.id !== id)
                this.setState({agents: newAgents})
            })
    }

    editButton(agentId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                size="sm"
                color="primary"
                tag={Link}
                to={"/agents/" + agentId}
            > Edit </Button>
        )
    }

    deleteButton(agentId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                size="sm"
                color="danger"
                onClick={() => this.delete(agentId)}
            > Delete </Button>
        )
    }

    createButton() {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button color="success" tag={Link} to="/agents/create">
                Create Agent
            </Button>
        )
    }

    viewDetailsButton(agentId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button color="success" tag={Link} to={"/agents/details/" + agentId}>
                View details
            </Button>
        )
    }

    render() {
        if (this.state.status >= 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        function intersperse(arr, sep) {
            if (arr.length === 0) {
                return [];
            }

            return arr.slice(1).reduce(function (xs, x) {
                return xs.concat([sep, x]);
            }, [arr[0]]);
        }

        const {agents, isLoading} = this.state;
        if (isLoading) {
            return <Loading/>
        }

        const agentsList = agents.map((agent) => {
            const languageSkills = intersperse(agent.languageSkills, ', ');
            const skills = intersperse(agent.skills, ', ');
            return (
                <tr key={agent.id}>
                    <td style={{whiteSpace: "nowrap"}}>{agent.codename}</td>
                    <td>{agent.activeSince} </td>
                    <td>{languageSkills}</td>
                    <td>{skills}</td>
                    <td>
                        <ButtonGroup>
                            {this.viewDetailsButton(agent.id)}
                        </ButtonGroup>
                    </td>
                    <td>
                        <ButtonGroup>
                            {this.editButton(agent.id)}
                            {this.deleteButton(agent.id)}
                        </ButtonGroup>
                    </td>
                </tr>
            );
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        {this.createButton()}
                    </div>
                    <h3>List of Agents</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="15%">Codename</th>
                            <th width="15%">Active since</th>
                            <th width="35%">Languages</th>
                            <th width="35%">Skills</th>
                        </tr>
                        </thead>
                        <tbody>{agentsList}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}