import React, {Component} from "react"
import {Button, Container, Form, FormGroup, Label} from "reactstrap";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import Loading from "../utilities/Loading";

/**
 * @author David Myska
 */
export default class ModifyAgent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            codename: "",
            activeSince: new Date().toISOString().substr(0,10),
            dutyEnded: "",
            languageSkills: [],
            skills: [],
            isLoading: true,
            isCreate: this.props.match.params.id === "create",
            status: 200
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        if (!this.state.isCreate) {
            const headerOptions = {
                method: "GET",
                headers: this.props.getAuthHeader()
            }
            await fetch(`/pa165/rest/agent/${this.props.match.params.id}`, headerOptions)
                .then(response => response.json())
                .then((data) => {
                    this.setState(data)
                    this.setState({isLoading: false})
                })
        } else {
            this.setState({isLoading: false})
        }
    }

    handleChange(event) {
        const {name, value} = event.target
        switch (name) {
            case "languageSkills":
                this.setState({[name]: value.split(",")})
                break
            case "skills":
                this.setState({[name]: value.split(",")})
                break
            default:
                this.setState({[name]: value})
                break
        }
    }

    async handleSubmit(event) {
        event.preventDefault()

        const agentItem = this.state

        const headerOptions = {
            method: agentItem.id ? "PUT" : "POST",
            headers: this.props.getAuthHeader(),
            body: JSON.stringify(agentItem)
        }
        await fetch("/pa165/rest/agent/" + (agentItem.id ? `/update/${agentItem.id}` : "create"), headerOptions)
        //this.props.history.push("/agents");
        this.props.history.goBack()
    }

    renderCodename() {
        if (!this.state.isCreate) {
            return null;
        }
        return (
            <FormGroup>
                <Label for="codename">Codename: </Label>
                <input
                    type="text"
                    name="codename"
                    placeholder="Tester"
                    required={true}
                    value={this.state.codename}
                    onChange={this.handleChange}
                />
            </FormGroup>
        )
    }

    renderDutyStart() {
        if (!this.state.isCreate) {
            return null
        }
        return (
            <FormGroup>
                <Label for="activeSince">Duty starts: </Label>
                <input
                    type="date"
                    name="activeSince"
                    required={true}
                    value={this.state.activeSince}
                    onChange={this.handleChange}
                />
            </FormGroup>
        )
    }

    renderDutyEnd() {
        if (this.props.match.params.id === "create") {
            return null
        }
        return (
            <FormGroup>
                <Label for="dutyEnded">Duty ended: </Label>
                <input
                    type="date"
                    name="dutyEnded"
                    value={this.state.dutyEnded}
                    onChange={this.handleChange}
                />
            </FormGroup>
        )
    }

    renderLanguageSkills() {
        return (
            <FormGroup>
                <Label for="languageSkills">Languages (separate with comma): </Label>
                <input
                    type="text"
                    name="languageSkills"
                    placeholder="Slovak, Czech, Chinese"
                    required={true}
                    value={this.state.languageSkills}
                    onChange={this.handleChange}
                />
            </FormGroup>
        )
    }

    renderGeneralSkills() {
        return (
            <FormGroup>
                <Label for="skills">Skills (separate with comma): </Label>
                <input
                    type="text"
                    name="skills"
                    placeholder="Weapons, special skills..."
                    required={true}
                    value={this.state.skills}
                    onChange={this.handleChange}
                />
            </FormGroup>
        )
    }

    render() {
        if (this.state.status >= 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        if (this.state.isLoading) {
            return <Loading/>
        }

        return (
            <div>
                <Container>
                    <h3>Agent {this.state.isCreate ? "" : this.state.codename}</h3>
                    <Form onSubmit={this.handleSubmit}>
                        {this.renderCodename()}
                        {this.renderDutyStart()}
                        {this.renderDutyEnd()}
                        {this.renderLanguageSkills()}
                        {this.renderGeneralSkills()}
                        <FormGroup>
                            <Button color="success" type="submit">
                                Save
                            </Button>
                            {' '}
                            <Button color="danger" onClick={this.props.history.goBack}>
                                Cancel
                            </Button>
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }
}