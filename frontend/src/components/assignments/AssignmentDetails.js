import React, {Component} from "react";
import {Button, Container, Table} from "reactstrap";
import {intersperse} from "../utilities/Helpers";
import Loading from "../utilities/Loading";

export default class AssignmentDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: "",
            agent: {},
            mission: {},
            start: "",
            finish: "",
            agentReport: "",
            operatorReport: "",
            isLoading: true,
            status: 200
        }
    }

    async componentDidMount() {
        this.setState({isLoading: true})

        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }
         await fetch(`/pa165/rest/assignment/${this.props.match.params.id}`, headerOptions)
             .then(response => response.json())
             .then((data) => {this.setState(data); this.setState({isLoading: false, status: data.status})})
    }

    render() {
        if (this.state.isLoading) {
            return <Loading/>
        }
        const languages = intersperse(this.state.agent.languageSkills, ", ")
        const skills = intersperse(this.state.agent.skills, ', ')
        const objectives = intersperse(this.state.mission.objectives, ", ")
        const reqSkills = intersperse(this.state.mission.requiredSkills, ", ")
        const resources = intersperse(this.state.mission.resources, ", ")
        return (
            <Container fluid>
                <h2>Assignment details</h2>
                <h3>Basic info</h3>
                <Table className="mt-4">
                    <tbody>
                    <tr><td>Start</td><td>{this.state.start}</td></tr>
                    <tr><td>Finish</td><td>{this.state.finish}</td></tr>
                    <tr><td>Operator Report</td><td>{this.state.operatorReport ? this.state.operatorReport : "None"}</td></tr>
                    <tr><td>Agent Report</td><td>{this.state.agentReport ? this.state.agentReport : "None"}</td></tr>
                    </tbody>
                </Table>
                <h3>Agent details</h3>
                <Table className="mt-4">
                    <tbody>
                    <tr><td>Agent Codename</td><td>{this.state.agent.codename}</td></tr>
                    <tr><td>Language Skills</td><td>{languages}</td></tr>
                    <tr><td>Skills</td><td>{skills}</td></tr>
                    </tbody>
                </Table>
                <h3>Mission details</h3>
                <Table className="mt-4">
                    <tbody>
                    <tr><td>Mission Name</td><td>{this.state.mission.name}</td></tr>
                    <tr><td>Country</td><td>{this.state.mission.country.name}</td></tr>
                    <tr><td>Start</td><td>{this.state.mission.start}</td></tr>
                    <tr><td>Deadline</td><td>{this.state.mission.deadline}</td></tr>
                    <tr><td>Objectives</td><td>{objectives}</td></tr>
                    <tr><td>Required Skills</td><td>{reqSkills}</td></tr>
                    <tr><td>Resources</td><td>{resources}</td></tr>
                    </tbody>
                </Table>
                <Button color="info" onClick={() => this.props.history.goBack()}>
                    Go Back
                </Button>
            </Container>
        )
    }
}