import React, {Component} from "react"
import {Button, ButtonGroup, Container, Table} from "reactstrap";
import {Link} from "react-router-dom";
import Loading from "../utilities/Loading";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import {ViewDetailsButton} from "../utilities/Buttons";

export default class AssignmentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assignments: [],
            isLoading: true,
            status: 200,
            fetchId: 0
        }
        this.delete = this.delete.bind(this)
    }

    async componentDidMount() {
        this.setState({isLoading: true});

        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }
        fetch("/pa165/rest/assignment/all", headerOptions)
            .then(response => response.json())
            .then((data) => {
                console.log(data)
                this.setState({assignments: data, status: data.status, isLoading: false})
            });

        if (this.props.user.role !== "admin") {
            await fetch(`/pa165/rest/agent/myid`, headerOptions)
                .then(response => response.json())
                .then((data) => this.setState( {fetchId : data}))
        }
        console.log(this.state.fetchId)
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }
        await fetch("/pa165/rest/assignment/" + id, headerOptions)
            .then(() => {
                const newAssignments = this.state.assignments.filter((assignment) => assignment.id !== id)
                this.setState({assignments: newAssignments})
            })
    }

    editButton(assignmentId, agentId) {
        if (this.state.fetchId !== agentId && this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                color="primary"
                tag={Link}
                to={"/assignments/" + assignmentId}
            > Edit </Button>
        )
    }

    deleteButton(assignmentId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                color="danger"
                onClick={() => this.delete(assignmentId)}
            > Delete </Button>
        )
    }

    createButton() {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button color="success" tag={Link} to="/assignments/create">
                Create Assignment
            </Button>
        )
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }

        const {assignments, isLoading} = this.state;
        if (isLoading) {
            return <Loading/>
        }

        const assignmentsList = assignments.map((assignment) => {
            return (
                <tr key={assignment.id}>
                    <td>{assignment.mission.name}</td>
                    <td>{assignment.agent.codename}</td>
                    <td>{assignment.start} </td>
                    <td>{assignment.finish}</td>
                    <td>
                        <ButtonGroup>
                            { ViewDetailsButton("/assignments/details/" + assignment.id) }
                            {this.editButton(assignment.id, assignment.agent.id)}
                            {this.deleteButton(assignment.id)}
                        </ButtonGroup>
                    </td>
                </tr>
            );
        });
        console.log(assignmentsList)

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        {this.createButton()}
                    </div>
                    <h3>List of Assignments</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="30%">Mission</th>
                            <th width="30%">Agent</th>
                            <th width="20%">Start</th>
                            <th width="20%">Finish</th>
                        </tr>
                        </thead>
                        <tbody>{assignmentsList}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}