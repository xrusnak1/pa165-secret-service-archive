import React, {Component} from "react"
import {Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import Loading from "../utilities/Loading";
import Select from "react-select";

export default class ModifyAssignment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            assignment: {
                id: "",
                agentId: 0,
                missionId: 0,
                start: "",
                finish: "",
                agentReport: "",
                operatorReport: "",
            },
            agents: [],
            missions: [],
            isLoading: true,
            isCreate: this.props.match.params.id === "create",
            status: 200
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeSelectMission = this.handleChangeSelectMission.bind(this)
        this.handleChangeSelectAgent = this.handleChangeSelectAgent.bind(this)
    }

    async componentDidMount() {
        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }
        if (!this.state.isCreate) {
            await fetch(`/pa165/rest/assignment/${this.props.match.params.id}`, headerOptions)
                .then(response => response.json())
                .then((data) => {
                    this.setState({assignment: data})
                    this.setState({isLoading: false})
                })
        } else {
            await fetch('/pa165/rest/agent/all', headerOptions)
                .then(response => response.json())
                .then((data) => this.setState( { agents: data }));
            await fetch('/pa165/rest/mission/all', headerOptions)
                .then(response => response.json())
                .then((data) => this.setState( { missions: data }));
            this.setState({isLoading: false})
        }
    }

    handleChange(event) {
        const {name, value} = event.target
        const currentAssignment = this.state.assignment
        currentAssignment[name] = value
        this.setState({assignment : currentAssignment})
    }

    handleDateChange(event) {
        const {name, value} = event.target
        let assignment
        if (name === "start") {
            assignment = Object.assign({}, this.state.assignment, { start: value });
        }
        if (name === "finish") {
            assignment = Object.assign({}, this.state.assignment, { finish: value });
        }
        this.setState({assignment: assignment})
    }

    handleChangeSelectAgent(option) {
        const { assignment } = this.state
        assignment.agentId = option.value
        this.setState({ assignment: assignment })
    }

    handleChangeSelectMission(option) {
        const { assignment } = this.state
        assignment.missionId = option.value
        this.setState({ assignment: assignment })
    }

    async handleSubmit(event) {
        event.preventDefault()

        const assignmentItem = this.state.assignment

        const headerOptions = {
            method: assignmentItem.id ? "PUT" : "POST",
            headers: this.props.getAuthHeader(),
            body: JSON.stringify(assignmentItem)
        }
        await fetch("/pa165/rest/assignment/" + (assignmentItem.id ? `/${assignmentItem.id}` : "create"), headerOptions)
        this.props.history.goBack()
    }

    reportFormGroup() {
        console.log(this.state.assignment.operatorReport)
        if (!this.state.isCreate) {
            if (this.props.user.role === "admin") {
                return (
                    <FormGroup>
                        <Label for="finish">Operator Report:</Label>
                        <Input
                            type="textarea"
                            name="operatorReport"
                            placeholder="Operator Report"
                            value={this.state.assignment.operatorReport || ""}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                )
            }
            return (
                <FormGroup>
                    <Label for="finish">Agent Report:</Label>
                    <Input
                        type="textarea"
                        name="agentReport"
                        placeholder="Agent Report"
                        value={this.state.assignment.agentReport || ""}
                        onChange={this.handleChange}
                    />
                </FormGroup>
            )
        }
        return null
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        if (this.state.isLoading) {
            return <Loading/>
        }

        const { missions, agents, assignment } = this.state
        let selectAgent = agents.map((agent) => {
            return {
                value: agent.id,
                label: agent.codename
            }
        })
        let selectMission = missions.map((mission) => {
            return {
                value: mission.id,
                label: mission.name
            }
        })

        return (
            <div>
                <Container>
                    <h3>Assignment {this.state.isCreate ? "" : this.state.id}</h3>
                    <Form onSubmit={this.handleSubmit}>
                        {this.state.isCreate ? <FormGroup>
                            <Label for="agent">Agent: </Label>
                            <Select name="agent"
                                    required={true}
                                    options={selectAgent}
                                    value={selectAgent.find(option => option.value === assignment.agentId)}
                                    onChange={this.handleChangeSelectAgent}>
                            </Select>
                        </FormGroup> : null }
                        {this.state.isCreate ? <FormGroup>
                            <Label for="mission">Mission: </Label>
                            <Select name="mission"
                                    required={true}
                                    options={selectMission}
                                    value={selectMission.find(option => option.value === assignment.missionId)}
                                    onChange={this.handleChangeSelectMission}>
                            </Select>
                        </FormGroup> : null }
                        { this.props.user.role === "admin" ?
                            <div>
                            <FormGroup>
                                <Label for="start">Assignment starts: </Label>
                                <input
                                    type="date"
                                    name="start"
                                    required={true}
                                    value={this.state.assignment.start}
                                    onChange={this.handleDateChange}
                                />
                                <Label for="start">{this.state.assignment.report} </Label>
                            </FormGroup>
                            <FormGroup>
                            <Label for="finish">Assignment finish: </Label>
                            <input
                            type="date"
                            name="finish"
                            required={true}
                            value={this.state.assignment.finish}
                            onChange={this.handleDateChange}
                            />
                            </FormGroup> </div>: null}
                        {this.reportFormGroup()}
                        <FormGroup>
                            <Button color="success" type="submit">
                                Save
                            </Button>
                            {' '}
                            <Button color="danger" onClick={() => this.props.history.goBack()}>
                                Cancel
                            </Button>
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }
}