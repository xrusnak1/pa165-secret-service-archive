import React, {Component} from "react"
import {Button, ButtonGroup, Container, Table} from "reactstrap";
import {Link} from "react-router-dom";
import Loading from "../utilities/Loading";
import BasicErrorHandler from "../utilities/BasicErrorHandler";

export default class CountriesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            isLoading: true,
            status: 200
        }
        this.delete = this.delete.bind(this)
    }

    componentDidMount() {
        this.setState({isLoading: true});

        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }

        fetch("/pa165/rest/country/all", headerOptions)
            .then(response => response.json())
            .then((data) => this.setState({countries: data, status: data.status, isLoading: false}));
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }
        await fetch("/pa165/rest/country/" + id, headerOptions)
            .then(() => {
                const newCountries = this.state.countries.filter((country) => country.id !== id)
                this.setState({countries: newCountries})
            })
    }

    editAndDeleteButton(countryId) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
        <ButtonGroup>
            <Button
                size="sm"
                color="primary"
                tag={Link}
                to={"/countries/" + countryId}
            > Edit </Button>
            <Button
                size="sm"
                color="danger"
                onClick={() => this.delete(countryId)}
            > Delete </Button>
        </ButtonGroup>
        )
    }

    createCountryButton() {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button color="success" tag={Link} to="/countries/create">
                Create Country
            </Button>
        )
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        function intersperse(arr, sep) {
            if (arr.length === 0) {
                return [];
            }

            return arr.slice(1).reduce(function (xs, x) {
                return xs.concat([sep, x]);
            }, [arr[0]]);
        }

        const {countries, isLoading} = this.state;
        if (isLoading) {
            return <Loading/>
        }

        let languages;
        const countriesList = countries.map((country) => {
            languages = intersperse(country.languages, ', ');
            return (
                <tr key={country.id}>
                    <td style={{whiteSpace: "nowrap"}}>{country.name}</td>
                    <td>{languages}</td>
                    <td>{country.literacy} </td>
                    <td>
                        {this.editAndDeleteButton(country.id)}
                    </td>
                </tr>
            );
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        {this.createCountryButton()}
                    </div>
                    <h3>List of Countries</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="30%">Name</th>
                            <th width="35%">Languages</th>
                            <th width="35%">Literacy</th>
                        </tr>
                        </thead>
                        <tbody>{countriesList}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}