import React, {Component} from "react"
import {Button, Container, Form, FormGroup, Label} from "reactstrap";
import {Link} from "react-router-dom";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import Loading from "../utilities/Loading";

export default class ModifyCountry extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            languages: [],
            literacy: 0,
            isLoading: true,
            status: 200
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        if (this.props.match.params.id !== "create") {
            const headerOptions = {
                method: "GET",
                headers: this.props.getAuthHeader()
            }
            await fetch(`/pa165/rest/country/${this.props.match.params.id}`, headerOptions)
                .then(response => response.json())
                .then((data) => {
                    this.setState(data)
                    this.setState({isLoading: false})
                })
        } else {
            this.setState({isLoading: false})
        }
    }

    handleChange(event) {
        const {name, value} = event.target
        name === "languages" ? this.setState({[name]: value.split(",")}) : this.setState({[name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault()

        const countryItem = this.state

        const headerOptions = {
            method: countryItem.id ? "PUT" : "POST",
            headers: this.props.getAuthHeader(),
            body: JSON.stringify(countryItem)
        }
        await fetch("/pa165/rest/country/" + (countryItem.id ? `/${countryItem.id}` : "create"), headerOptions)
        this.props.history.push("/countries");
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        if (this.state.isLoading) {
            return <Loading/>
        }
        return (
            <div>
                <Container>
                    <h3>Add Country</h3>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label for="countryName">Country name: </Label>
                            <input
                                type="text"
                                name="name"
                                required={true}
                                placeholder="Slovakia"
                                value={this.state.name}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="languages">Languages (separate with comma): </Label>
                            <input
                                type="text"
                                name="languages"
                                placeholder="Slovak, Czech, Chinese"
                                value={this.state.languages}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="literacy">Literacy:</Label>
                            <input
                                type="number"
                                name="literacy"
                                placeholder="0"
                                min="0"
                                step="0.01"
                                value={this.state.literacy}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Button color="success" type="submit">
                                Save
                            </Button>
                            {' '}
                            <Button color="danger" tag={Link} to="/countries">
                                Cancel
                            </Button>
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }
}