import React, {Component} from "react";
import Loading from "../utilities/Loading";
import {Button, Container, Table} from "reactstrap";
import {intersperse} from "../utilities/Helpers";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import {EditButton} from "../utilities/Buttons";

export default class MissionDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            country: {},
            start: "",
            deadline: "",
            assignments: [],
            resources: [],
            objectives: [],
            suitableAgents: [],
            requiredSkills: [],
            isLoading: true,
            status: 200
        }

        this.delete = this.delete.bind(this);
    }

    componentDidMount() {
        this.setState({isLoading: true});
        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }

        fetch(`/pa165/rest/mission/details/${this.props.match.params.id}`, headerOptions)
            .then(response => response.json())
            .then((data) => {this.setState(data); this.setState({isLoading: false, status: data.status})});

        fetch(`/pa165/rest/mission/${this.props.match.params.id}/suitableagents`, headerOptions)
            .then(response => response.json())
            .then((data) => this.setState({suitableMissions: Array.from(data), status: data.status}));
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }
        await fetch("/pa165/rest/mission/delete/" + id, headerOptions)
            .then(() => {
                const newMissions = this.state.missions.filter((mission) => mission.id !== id)
                this.setState({missions: newMissions})
            })
    }

    suitableMissionsTable() {
        if (!this.state.suitableAgents || this.state.suitableAgents.length === 0) {
            return "No suitable agents for this mission."
        }
        return <Table>
                <thead>
                    <tr>
                        <th>Agent codename</th>
                        <th>Is active since</th>
                        <th>End of duty</th>
                        <th>Known languages</th>
                        <th>Acquired skills</th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.state.suitableAgents.map(agent => {
                        return (
                            <tr key={agent.id}>
                                <td>{agent.codename}</td>
                                <td>{agent.activeSince}</td>
                                <td>{agent.dutyEnded}</td>
                                <td>{intersperse(agent.languageSkills, ', ')}</td>
                                <td>{intersperse(agent.skills, ', ')}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
        </Table>
    }

    DeleteButton(id) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                size="sm"
                color="danger"
                onClick={() => this.delete(id)}
            > Delete </Button>
        )
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        const {id, name, country, start, deadline, assignments,
            resources, objectives, requiredSkills, isLoading} = this.state;

        if (isLoading) {
            return <Loading/>
        }
        const requiredResources = intersperse(resources, ', ')
        const totalObjectives = intersperse(objectives, ', ')
        const skills = intersperse(requiredSkills, ', ')

        const suitableAgentsTable = this.suitableMissionsTable();

        const assignmentsLines = assignments.map((assignment) => {
            return (
                <tr key={assignment.id}>
                    <td>{assignment.agent.codename}</td>
                    <td>{assignment.start} </td>
                    <td>{assignment.finish}</td>
                    <td>{intersperse(assignment.agent.languageSkills, ', ')}</td>
                    <td>{intersperse(assignment.agent.skills, ', ')}</td>
                </tr>
            );
        })

        return (
            <div>
                <Container fluid>
                    <h2>Mission details</h2>
                    { EditButton(this.props.user.role, "/missions/" + id) }
                    { this.DeleteButton(id) }
                    <h3>Basic info</h3>
                    <Table className="mt-4">
                        <tbody>
                        <tr><td>Name</td><td>{name}</td></tr>
                        <tr><td>Country</td><td>{country.name}</td></tr>
                        <tr><td>Start</td><td>{start}</td></tr>
                        <tr><td>Deadline</td><td>{deadline}</td></tr>
                        <tr><td>Resources</td><td>{requiredResources}</td></tr>
                        <tr><td>Objectives</td><td>{totalObjectives}</td></tr>
                        <tr><td>Skills</td><td>{skills}</td></tr>
                        </tbody>
                    </Table>
                    <h3>Suitable agents</h3>
                    {suitableAgentsTable}
                    <h3>Assignments</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th>Agent codename</th>
                            <th>Assigned since</th>
                            <th>Assignment ended</th>
                            <th>Agent known languages</th>
                            <th>Agent acquired skills</th>
                        </tr>
                        </thead>
                        <tbody>{assignmentsLines}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}