import React, {Component} from "react";
import Loading from "../utilities/Loading";
import {Button, ButtonGroup, Container, Table} from "reactstrap";
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import {CreateButton, EditButton, ViewDetailsButton} from "../utilities/Buttons";
import {intersperse} from "../utilities/Helpers";

export default class MissionsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            missions: [],
            isLoading: true,
            status: 200
        }
        this.delete = this.delete.bind(this);
    }

    componentDidMount() {
        this.setState({isLoading: true});
        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }

        fetch("/pa165/rest/mission/all", headerOptions)
            .then(response => response.json())
            .then((data) => this.setState({missions: data, status: data.status, isLoading: false}));
    }

    async delete(id) {
        const headerOptions = {
            method: "DELETE",
            headers: this.props.getAuthHeader()
        }
        await fetch("/pa165/rest/mission/delete/" + id, headerOptions)
            .then(() => {
                const newMissions = this.state.missions.filter((mission) => mission.id !== id)
                this.setState({missions: newMissions})
            })
    }

    DeleteButton(id) {
        if (this.props.user.role !== "admin") {
            return null
        }
        return (
            <Button
                size="sm"
                color="danger"
                onClick={() => this.delete(id)}
            > Delete </Button>
        )
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }

        const {missions, isLoading} = this.state;
        if (isLoading) {
            return <Loading/>
        }

        const missionsList = missions.map((mission) => {
            const resources = intersperse(mission.resources, ', ');
            const requiredSkills = intersperse(mission.requiredSkills, ', ');
            const objectives = intersperse(mission.objectives, ', ')
            return (
                <tr key={mission.id}>
                    <td style={{whiteSpace: "nowrap"}}>{mission.name}</td>
                    <td>{mission.country.name}</td>
                    <td>{mission.start}</td>
                    <td>{mission.deadline}</td>
                    <td>{resources}</td>
                    <td>{requiredSkills}</td>
                    <td>{objectives}</td>
                    <td>
                        <ButtonGroup>
                            { ViewDetailsButton("/missions/details/" + mission.id) }
                        </ButtonGroup>
                    </td>
                    <td>
                        <ButtonGroup>
                            { EditButton(this.props.user.role, "/missions/" + mission.id) }
                            { this.DeleteButton(mission.id) }
                        </ButtonGroup>
                    </td>
                </tr>
            );
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        { CreateButton(this.props.user.role, "/missions/create", "Mission") }
                    </div>
                    <h3>List of Missions</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="15%">Name</th>
                            <th width="15%">Country</th>
                            <th width="10%">Start</th>
                            <th width="10%">End</th>
                            <th width="17%">Resources</th>
                            <th width="17%">Required skills</th>
                            <th width="16%">Objectives</th>
                        </tr>
                        </thead>
                        <tbody>{missionsList}</tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}