import React, {Component} from "react";
import {Container, Form, FormGroup, Label} from "reactstrap";
import Select from 'react-select'
import BasicErrorHandler from "../utilities/BasicErrorHandler";
import Loading from "../utilities/Loading";
import {CancelButton, SaveButton} from "../utilities/Buttons";

export default class ModifyMission extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mission: {
                id: "",
                name: "",
                country: {},
                start: "",
                deadline: "",
                resources: [],
                objectives: [],
                requiredSkills: []
            },
            countries: [],
            status: 200,
            isCreate: this.props.match.params.id === "create",
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeSelect = this.handleChangeSelect.bind(this)
    }

    async componentDidMount() {
        const headerOptions = {
            method: "GET",
            headers: this.props.getAuthHeader()
        }

        if (!this.state.isCreate) {
            await fetch(`/pa165/rest/mission/${this.props.match.params.id}`, headerOptions)
                .then(response => response.json())
                .then((data) => this.setState({ mission: data }));
        } else {
            await fetch('/pa165/rest/country/all', headerOptions)
                .then(response => response.json())
                .then((data) => this.setState( { countries: data, isLoading: false }));
        }
    }

    handleChange(event) {
        const {name, value} = event.target
        const currentMission = this.state.mission
        switch (name) {
            case "resources":
            case "objectives":
            case "requiredSkills":
                currentMission[name] = value.split(",")
                break
            default:
                currentMission[name] = value
        }
        this.setState({ mission: currentMission })
    }

    handleChangeSelect(option) {
        const { mission, countries } = this.state
        mission.country = countries.find((country) => country.id === option.value)
        this.setState({ mission: mission })
    }

    async handleSubmit(event) {
        event.preventDefault()

        const missionItem = this.state.mission

        const headerOptions = {
            method: missionItem.id ? "PUT" : "POST",
            headers: this.props.getAuthHeader(),
            body: JSON.stringify(missionItem)
        }
        await fetch("/pa165/rest/mission" + (missionItem.id ? `/update/${missionItem.id}` : "/create"), headerOptions)
        this.props.history.goBack()
    }

    render() {
        if (this.state.status > 400) {
            return <BasicErrorHandler status={this.state.status} user={this.props.user}/>
        }
        if (this.state.isLoading) {
            return <Loading/>
        }

        const { mission, countries} = this.state
        let options = countries.map((country) => {
            return {
                value: country.id,
                label: country.name
            }
        })
        return (
            <div>
                <Container>
                    <h3>Add Mission</h3>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label for="name">Name: </Label>
                            <input
                                type="text"
                                name="name"
                                required={true}
                                placeholder="Operation Alpha"
                                value={mission.name}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="country">Country: </Label>
                            <Select name="country"
                                    options={options}
                                    value={options.find(option => option.value === mission.country.id)}
                                    onChange={this.handleChangeSelect}>
                            </Select>
                        </FormGroup>
                        <FormGroup>
                            <Label for="start">Mission start: </Label>
                            <input
                                type="date"
                                name="start"
                                required={true}
                                value={mission.start}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="deadline">Mission deadline: </Label>
                            <input
                                type="date"
                                name="deadline"
                                required={true}
                                value={mission.deadline}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="resources">Resources (separate with comma): </Label>
                            <input
                                type="text"
                                name="resources"
                                placeholder="BFG"
                                value={mission.resources}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="objectives">Objectives (separate with comma): </Label>
                            <input
                                type="text"
                                name="objectives"
                                placeholder="Have fun"
                                value={mission.objectives}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="requiredSkills">Required skills (separate with comma): </Label>
                            <input
                                type="text"
                                name="requiredSkills"
                                placeholder="English"
                                value={mission.requiredSkills}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            { SaveButton() }
                            {' '}
                            { CancelButton("/missions") }
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }

}