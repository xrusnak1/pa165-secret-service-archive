import React, {Component} from 'react'
import {Nav, Navbar, NavbarBrand, NavLink} from "reactstrap";

export default class AppNavbar extends Component {

    renderLogin() {
        if (this.props.user.isLoggedIn)
            return <NavLink href='/logout' className='nav-link'>LogOut</NavLink>
        return <NavLink href='/auth' className='nav-link'>LogIn</NavLink>
    }

    renderProfile() {
        if (this.props.user.role === "admin")
            return null
        return <NavLink href='/agents/details/profile' className='nav-link'>Profile</NavLink>
    }

    renderAssignments() {
        if (this.props.user.role !== "admin") {
            return null
        }
        return <NavLink href='/assignments' className='nav-link'>Assignments</NavLink>
    }

    renderGuest() {
        return (
            <Nav className='mr-auto navbar-nav'>
                {this.renderLogin()}
            </Nav>
        )
    }

    renderAuthenticated() {
        return (
            <Nav className='mr-auto navbar-nav'>
                <NavLink href='/countries' className='nav-link'>Countries</NavLink>
                <NavLink href='/agents' className='nav-link'>Agents</NavLink>
                <NavLink href='/missions' className='nav-link'>Missions</NavLink>
                {this.renderAssignments()}
                {this.renderProfile()}
                {this.renderLogin()}
            </Nav>
        )
    }

    render() {
        return (
            <Navbar color='dark' dark expand='md'>
                <NavbarBrand href='/'>Home</NavbarBrand>
                {this.props.user.isLoggedIn ? this.renderAuthenticated() : this.renderGuest()}
            </Navbar>
        )
    }
}