import React, {Component} from "react"
import Cookies from "universal-cookie"
import LogInForm from "../../pages/LogInForm";

/**
 * @author David Myska
 */
export default class Auth extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            token: "",
            role: "",
            error: ""
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        const {name, value} = event.target
        this.setState({[name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault()

        const credentials = { username: this.state.username, password: this.state.password }

        const headerOptions = {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(credentials)
        }

        let status = 200
        await fetch("/pa165/rest/auth", headerOptions)
            .then((response) => response.json())
            .then((data) => {
                this.setState(data)
                status = data.status
            })
        console.log("status: " + status)

        if (status > 400) {
            this.setState({error: "Incorrect credentials!"})
            return
        }

        const cookies = new Cookies();
        cookies.set('jwt', JSON.stringify({
            token: this.state.token,
            username: this.state.username,
            role: this.state.role }), {path: '/', maxAge: 3600})

        this.props.myUpdate();
        this.props.history.push("/");
    }

    render() {
        return (
            <LogInForm handleSubmit={this.handleSubmit}
                       handleChange={this.handleChange}
                       username={this.state.username}
                       password={this.state.password}
                       error={this.state.error}
            />
        )
    }
}