import React from "react";
import {Redirect} from "react-router-dom";

/**
 * @author David Myska
 */
export default function BasicErrorHandler(props) {
    if (props.status === 403 && !props.user.isLoggedIn)
        return <Redirect to={'/auth'}/>
    if (props.status === 403 && props.user.isLoggedIn)
        return <Redirect to={'/accessdenied'}/>
    return <Redirect to={'/totalerrorfromwhichcannotrecover'}/>
}