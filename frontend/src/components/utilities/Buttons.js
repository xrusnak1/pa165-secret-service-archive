import {Button} from "reactstrap";
import {Link} from "react-router-dom";
import React from "react";

export function EditButton(role, to) {
    if (role !== "admin") {
        return null
    }
    return (
        <Button
            size="sm"
            color="primary"
            tag={Link}
            to={to}
        > Edit </Button>
    )
}

export function CreateButton(role, to, what) {
    if (role !== "admin") {
        return null
    }
    return (
        <Button color="success" tag={Link} to={to}>
            Create {what}
        </Button>
    )
}

export function ViewDetailsButton(to) {
    return (
        <Button color="success" tag={Link} to={to}>
            View details
        </Button>
    )
}

export function SaveButton() {
    return (
        <Button color="success" type="submit">
            Save
        </Button>
    )
}

export function CancelButton(to) {
    return (
        <Button color="danger" tag={Link} to={to}>
            Cancel
        </Button>
    )
}