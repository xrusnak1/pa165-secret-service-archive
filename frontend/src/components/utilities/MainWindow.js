import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "../../pages/Home";
import CountriesList from "../countries/CountriesList";
import ModifyCountry from "../countries/ModifyCountry";
import AgentsList from "../agents/AgentsList";
import ModifyAgent from "../agents/ModifyAgent";
import AssignmentsList from "../assignments/AssignmentsList";
import ModifyAssignment from "../assignments/ModifyAssignment";
import Auth from "./Auth";
import {LogOut} from "../../pages/LogOut";
import {BasicError} from "../../pages/BasicError";
import {Error403} from "../../pages/Error403";
import AgentDetails from "../agents/AgentDetails";
import MissionsList from "../missions/MissionsList";
import ModifyMission from "../missions/ModifyMission";
import MissionDetails from "../missions/MissionDetails";
import AssignmentDetails from "../assignments/AssignmentDetails";

export default class MainWindow extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} render={
                        (props) => <Home {...props}
                                         getAuthHeader={this.props.getAuthHeader}
                                         user={this.props.user}/>}/>
                    <Route path='/countries' exact={true} render={
                        (props) => <CountriesList {...props}
                                                  getAuthHeader={this.props.getAuthHeader}
                                                  user={this.props.user}/>}/>
                    <Route path='/countries/:id' exact={true} render={
                        (props) => <ModifyCountry {...props}
                                                  getAuthHeader={this.props.getAuthHeader}
                                                  user={this.props.user}/>}/>
                    <Route path='/agents' exact={true} render={
                        (props) => <AgentsList {...props}
                                               getAuthHeader={this.props.getAuthHeader}
                                               user={this.props.user}/>}/>
                    <Route path='/agents/:id' exact={true} render={
                        (props) => <ModifyAgent {...props}
                                                getAuthHeader={this.props.getAuthHeader}
                                                user={this.props.user}/>}/>
                    <Route path='/assignments' exact={true} render={
                        (props) => <AssignmentsList {...props}
                                               getAuthHeader={this.props.getAuthHeader}
                                               user={this.props.user}/>}/>
                    <Route path='/assignments/:id' exact={true} render={
                        (props ) => <ModifyAssignment {...props}
                                                getAuthHeader={this.props.getAuthHeader}
                                                user={this.props.user}/>}/>
                    <Route path='/assignments/details/:id' exact={true} render={
                        (props ) => <AssignmentDetails {...props}
                                                      getAuthHeader={this.props.getAuthHeader}
                                                      user={this.props.user}/>}/>
                    <Route path='/agents/details/:id' exact={true} render={
                        (props) => <AgentDetails {...props}
                                                      getAuthHeader={this.props.getAuthHeader}
                                                      user={this.props.user}/>}/>
                    <Route path='/missions' exact={true} render={
                        (props) => <MissionsList {...props}
                                               getAuthHeader={this.props.getAuthHeader}
                                               user={this.props.user}/>}/>
                    <Route path='/missions/:id' exact={true} render={
                        (props) => <ModifyMission {...props}
                                                getAuthHeader={this.props.getAuthHeader}
                                                user={this.props.user}/>}/>
                    <Route path='/missions/details/:id' exact={true} render={
                        (props) => <MissionDetails {...props}
                                                getAuthHeader={this.props.getAuthHeader}
                                                user={this.props.user}/>}/>
                    <Route path='/auth' exact={true} render={
                        (props) => <Auth {...props}
                                         myUpdate={this.props.updateHandle}/> }/>
                    <Route path='/logout' exact={true} render={
                        (props) => <LogOut {...props}
                                           myUpdate={this.props.updateHandle}/> }/>
                    <Route path='/accessdenied' exact={true} render={(props) => <Error403 {...props}/> }/>
                    <Route path='*' exact={true} render={(props) => <BasicError {...props}/> }/>
                </Switch>
            </Router>
        )
    }
}