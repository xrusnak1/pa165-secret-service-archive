import {Component} from "react";

/**
 * @author David Myska
 */
export class BasicError extends Component {
    render() {
        return (
            <div>
                <h3>Oops, sorry. Something went wrong.</h3>
            </div>
        )
    }
}
