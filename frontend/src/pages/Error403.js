import {Component} from "react";

/**
 * @author David Myska
 */
export class Error403 extends Component {
    render() {
        return (
            <div>
                <h3>You don't have permission to access that.</h3>
            </div>
        )
    }
}
