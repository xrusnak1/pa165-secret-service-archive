import React, {Component} from 'react'

export default class Home extends Component {

    render() {
        return this.welcomeMessage()
    }

    welcomeMessage() {
        let jwt = this.props.user
        if (jwt.isLoggedIn) {
            return (
                <div>
                    <h1>Welcome, testers!</h1>
                    <p>Username: {jwt.username}</p>
                    <p>Role: {jwt.role}</p>
                </div>
            )
        }
        return (
            <div>
                <h1>Welcome, testers!</h1>
                <p>Default available account - username: "admin", password: "admin"</p>
                <p>Any created agent has login credentials - username: "agent's codename", password: "agent's codename"</p>
            </div>
        )
    }
}