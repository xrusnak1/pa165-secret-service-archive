import React, {Component} from 'react'
import {Button, Container, Form, FormGroup, Label} from "reactstrap";

/**
 * @author David Myska
 */
export default class LogInForm extends Component {
    render() {
        return (
            <div>
                <Container>
                    <h3>LogIn</h3>
                    <Form onSubmit={this.props.handleSubmit}>
                        <FormGroup>
                            <Label for="username">Username: </Label>
                            <input
                                type="text"
                                name="username"
                                placeholder="Your username"
                                value={this.props.username}
                                onChange={this.props.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Password: </Label>
                            <input
                                type="password"
                                name="password"
                                value={this.props.password}
                                onChange={this.props.handleChange}
                            />
                        </FormGroup>
                        <p style={{color: "red"}}>{this.props.error}</p>
                        <FormGroup>
                            <Button color="success" type="submit">
                                LogIn
                            </Button>
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }
}