import {Component} from "react";
import Cookies from "universal-cookie";

/**
 * @author David Myska
 */
export class LogOut extends Component {
    render() {
        this.destroyCookies()
        return (
            <div>
                <h3>You have been logged out.</h3>
            </div>
        )
    }

    destroyCookies() {
        const cookies = new Cookies();
        cookies.remove('jwt', {path: '/'})
    }
}
