package cz.muni.fi.pa165;

import cz.muni.fi.pa165.dto.LoginCredentialsDto;
import cz.muni.fi.pa165.facade.AuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

/**
 * @author David Rusnak
 */
@Configuration
@EnableSwagger2
public class Config {

    Environment environment;

    private WebInit webInit;

    private AuthenticationFacade authenticationFacade;

    @Autowired
    public Config(WebInit webInit, AuthenticationFacade authenticationFacade, Environment environment) {
        this.webInit = webInit;
        this.authenticationFacade = authenticationFacade;
        this.environment = environment;
    }

    @Bean
    @Profile("devel")
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    @EventListener(ApplicationStartedEvent.class)
    public void printSwaggerUI() {
        if (Arrays.asList(environment.getActiveProfiles()).contains("devel")) {
            System.out.println("SWAGGER UI: http://localhost:8080/pa165/rest/swagger-ui/");
        }
    }

    @EventListener(ApplicationStartedEvent.class)
    public void fillDemoData() {
        webInit.createDemoData();
    }

    @EventListener(ApplicationStartedEvent.class)
    public void addAdminUser() {
        var admin = new LoginCredentialsDto();
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.setRole("admin");
        authenticationFacade.create(admin);
    }
}
