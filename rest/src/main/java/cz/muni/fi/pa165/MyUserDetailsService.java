package cz.muni.fi.pa165;

import cz.muni.fi.pa165.facade.AuthenticationFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author David Myska
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

    final static Logger logger = LoggerFactory.getLogger(MyUserDetailsService.class);

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        try {
            var userCredentials = authenticationFacade.getUserCredentials(userName);
            return new User(userCredentials.getUsername(), userCredentials.getPassword(),
                    List.of(new SimpleGrantedAuthority("ROLE_" + userCredentials.getRole())));
        } catch (Exception e) {
            logger.info("Exception: " + e.getMessage());
            throw new UsernameNotFoundException("Username not found.");
        }
    }
}
