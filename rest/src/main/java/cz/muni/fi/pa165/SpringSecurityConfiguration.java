package cz.muni.fi.pa165;

import cz.muni.fi.pa165.utils.JwsRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author David Myska
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${security.on}")
    private String securityOn;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private JwsRequestFilter jwsRequestFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (securityOn.equals("true")) {
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/agent/create").hasRole("admin")
                    .antMatchers("/agent/delete/*").hasRole("admin")
                    .antMatchers("/agent/**").hasAnyRole("admin", "user")
                    .antMatchers("/country/create").hasRole("admin")
                    .antMatchers("/country/**").hasAnyRole("admin", "user")
                    .antMatchers("/mission/create").hasAnyRole("admin")
                    .antMatchers("/mission/**").hasAnyRole("admin", "user")
                    .antMatchers("/assignment/create").hasRole("admin")
                    .antMatchers("/assignment/**").hasAnyRole("admin", "user")
                    .antMatchers("/demo-data/**").hasAnyRole("admin")
                    .antMatchers("/auth").permitAll()
                    .antMatchers("/**").permitAll()
                    .anyRequest().authenticated()
                    .and().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        } else {
            http.csrf().disable().authorizeRequests().anyRequest().permitAll();
        }
        http.addFilterBefore(jwsRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService);
        auth.authenticationProvider(authProvider());
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(myUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
}