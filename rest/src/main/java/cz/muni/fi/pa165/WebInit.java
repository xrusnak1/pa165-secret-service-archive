package cz.muni.fi.pa165;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.CountryDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.facade.AgentFacade;
import cz.muni.fi.pa165.facade.CountryFacade;
import cz.muni.fi.pa165.facade.MissionFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author David Rusnak
 */
@Component
public class WebInit {

    final static Logger logger = LoggerFactory.getLogger(WebInit.class);

    CountryFacade countryFacade;

    AgentFacade agentFacade;

    MissionFacade missionFacade;

    private boolean demoGenerated = false;

    @Autowired
    public WebInit(CountryFacade countryFacade, AgentFacade agentFacade, MissionFacade missionFacade) {
        this.countryFacade = countryFacade;
        this.agentFacade = agentFacade;
        this.missionFacade = missionFacade;
    }

    public void createDemoData() {
        logger.info("createDemoData()");
        if (demoGenerated) {
            return;
        }
        createTestCountries();
        createTestAgents();
        createTestMissions();
        demoGenerated = true;
    }

    private CountryDto buildTestCountry(String name, Float literacy, Set<String> languages) {
        var countryDto = new CountryDto();
        countryDto.setName(name);
        countryDto.setLanguages(languages);
        countryDto.setLiteracy(literacy);
        return countryDto;
    }

    private void createTestCountries() {
        countryFacade.create(buildTestCountry("Slovakia", 0.74f, Set.of("sk")));
        countryFacade.create(buildTestCountry("Poland", 0.67f, Set.of("pl")));
        countryFacade.create(buildTestCountry("Russia", 0.84f, Set.of("ru")));
    }

    private AgentDto buildTestAgent(String codename, LocalDate activeSince, LocalDate dutyEnded, Set<String> skills,
                                    Set<String> languageSkills) {
        var agentDto = new AgentDto();
        agentDto.setCodename(codename);
        agentDto.setActiveSince(activeSince);
        agentDto.setDutyEnded(dutyEnded);
        agentDto.setSkills(skills);
        agentDto.setLanguageSkills(languageSkills);
        //a1Dto.setAssignments();
        return agentDto;
    }

    private void createTestAgents() {
        agentFacade.create(buildTestAgent("pearhead", LocalDate.of(2000, 11, 21),
                LocalDate.now(), Set.of("knife", "negotiation"), Set.of("ru", "pl", "sk")));
        agentFacade.create(buildTestAgent("bubblebutt", LocalDate.of(2004, 10, 1),
                null, Set.of("dance", "negotiation"), Set.of("hu", "pl", "ru")));
        agentFacade.create(buildTestAgent("perry", LocalDate.of(2007, 9, 3),
                null, Set.of("swimming", "tailslap"), Set.of("pl", "gr")));
    }

    private MissionDto buildTestMission(String name, CountryDto country, LocalDate start, LocalDate end,
                                        Set<String> objectives, Set<String> resources, Set<String> requiredSkills) {
        var missionDto = new MissionDto();
        missionDto.setName(name);
        missionDto.setCountry(country);
        missionDto.setStart(start);
        missionDto.setDeadline(end);
        missionDto.setObjectives(objectives);
        missionDto.setResources(resources);
        missionDto.setRequiredSkills(requiredSkills);

        return missionDto;
    }

    private void createTestMissions() {
        missionFacade.create(buildTestMission("Ka-Boom",
                countryFacade.findAll().stream().findFirst().get(),
                LocalDate.of(2001, 11, 21),
                LocalDate.of(2010, 5, 13),
                Set.of("Do stuff"),
                Set.of("RPG", "M15"),
                Set.of("pl", "gr")));

        missionFacade.create(buildTestMission("Shadow Moses",
                countryFacade.findAll().stream().collect(Collectors.toList()).get(1),
                LocalDate.of(2001, 11, 21),
                LocalDate.of(2010, 5, 13),
                Set.of("free hostages", "prevent nuclear strike"),
                Set.of("bandana", "cardboard box"),
                Set.of("en")));
    }
}
