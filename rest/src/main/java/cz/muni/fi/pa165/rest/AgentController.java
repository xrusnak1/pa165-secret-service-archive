package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.dto.AgentDetailsDto;
import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.facade.AgentFacade;
import cz.muni.fi.pa165.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author David Myska
 */
@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
@RequestMapping(value = "/agent")
public class AgentController {

    final static Logger logger = LoggerFactory.getLogger(AgentController.class);

    AgentFacade agentFacade;

    JwtUtils jwtUtils;

    @Autowired
    public AgentController(AgentFacade agentFacade, JwtUtils jwtUtils) {
        this.agentFacade = agentFacade;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping(path = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AgentDto> getAllAgents() {
        logger.info("getAllAgents()");
        return agentFacade.findAll();
    }

    @GetMapping(path = "/myid",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAgentId(@RequestHeader("Authorization") String token) {
        logger.info("getAgentId()");
        if (token == null)
            return ResponseEntity.status(403).build();
        token = token.substring(7);

        String username = jwtUtils.extractUsername(token);
        AgentDto agent = agentFacade.findByName(username);
        return ResponseEntity.ok(agent.getId());
    }

    @GetMapping(path = "/suitablemissions/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSuitableMissions(@PathVariable Long id, @RequestHeader("Authorization") String token) {
        logger.info("getSuitableMissions("+id.toString()+")");

        if (token == null)
            return ResponseEntity.status(403).build();
        token = token.substring(7);

        // If request from admin, it is ok
        if (jwtUtils.extractRole(token).equals("admin")) {
            return ResponseEntity.ok(agentFacade.findMissionsSuitableForAgent(id));
        }

        // Find which agent is asking for this information
        String username = jwtUtils.extractUsername(token);
        AgentDto agent = agentFacade.findByName(username);
        // If the agent is not asking for information about themself -> reject
        if (!agent.getId().equals(id)) {
            return ResponseEntity.status(403).build();
        }
        return ResponseEntity.ok(agentFacade.findMissionsSuitableForAgent(id));
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAgent(@PathVariable Long id, @RequestHeader("Authorization") String token) {
        logger.info("getAgent("+id.toString()+")");

        if (token == null)
            return ResponseEntity.status(403).build();
        token = token.substring(7);

        // If request from admin, it is ok
        if (jwtUtils.extractRole(token).equals("admin")) {
            return ResponseEntity.ok(agentFacade.find(id));
        }

        // Find which agent is asking for this information
        String username = jwtUtils.extractUsername(token);
        AgentDto agent = agentFacade.findByName(username);
        // If the agent is not asking for information about themself -> reject
        if (!agent.getId().equals(id)) {
            return ResponseEntity.status(403).build();
        }
        return ResponseEntity.ok(agent);
    }

    @GetMapping(path = "/details/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAgentDetails(@PathVariable Long id, @RequestHeader("Authorization") String token) {
        logger.info("getAgentDetails("+id.toString()+")");

        if (token == null)
            return ResponseEntity.status(403).build();
        token = token.substring(7);

        // If request from admin, it is ok
        if (jwtUtils.extractRole(token).equals("admin")) {
            var agent = agentFacade.find(id);
            return ResponseEntity.ok(agentFacade.findByName(agent.getCodename(), AgentDetailsDto.class));
        }

        // Find which agent is asking for this information
        String username = jwtUtils.extractUsername(token);
        AgentDetailsDto agent = agentFacade.findByName(username, AgentDetailsDto.class);
        // If the agent is not asking for information about themself -> reject
        if (!agent.getId().equals(id)) {
            return ResponseEntity.status(403).build();
        }
        return ResponseEntity.ok(agent);
    }

    @PutMapping(path = "/update/{id}")
    public AgentDto updateAgent(@Valid @RequestBody AgentDto agentDto) {
        logger.info("updateAgent()");
        return agentFacade.update(agentDto);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteAgent(@PathVariable Long id) {
        logger.info("deleteAgent()");
        agentFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "/create")
    public void createAgent(@Valid @RequestBody AgentDto agentDto) {
        logger.info("createAgent()");
        agentFacade.create(agentDto);
    }
}
