package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.dto.AssignmentCreateDto;
import cz.muni.fi.pa165.dto.AssignmentDto;
import cz.muni.fi.pa165.facade.AssignmentFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author Andrea Fickova
 */
@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
@RequestMapping(value = "/assignment")
public class AssignmentController {

    final static Logger logger = LoggerFactory.getLogger(AssignmentController.class);

    AssignmentFacade assignmentFacade;

    @Autowired
    public AssignmentController(AssignmentFacade assignmentFacade) {
        this.assignmentFacade = assignmentFacade;
    }

    @GetMapping(path = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AssignmentDto> getAllAssignments() {
        logger.info("getAllAssignments()");
        return assignmentFacade.findAll();
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AssignmentDto getAssignment(@PathVariable Long id) {
        logger.info("getAssignment("+id.toString()+")");
        return assignmentFacade.find(id);
    }

    @PutMapping(path = "/{id}")
    public AssignmentDto updateAssignment(@Valid @RequestBody AssignmentDto assignmentDto) {
        logger.info("updateAssignment()");
        return assignmentFacade.update(assignmentDto);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteAssignment(@PathVariable Long id) {
        logger.info("deleteAssignment()");
        assignmentFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "/create")
    public void createAssignment(@Valid @RequestBody AssignmentCreateDto assignmentDto) {
        logger.info("createAssignment()");
        assignmentFacade.createAssignment(assignmentDto);
    }
}
