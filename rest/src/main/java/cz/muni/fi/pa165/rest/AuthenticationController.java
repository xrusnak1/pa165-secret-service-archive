package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.MyUserDetailsService;
import cz.muni.fi.pa165.utils.AuthenticationRequest;
import cz.muni.fi.pa165.utils.AuthenticationResponse;
import cz.muni.fi.pa165.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
public class AuthenticationController {

    final static Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    private AuthenticationManager authenticationManager;

    private MyUserDetailsService userDetailsService;

    private JwtUtils jwtUtils;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager,
                                    MyUserDetailsService myUserDetailsService,
                                    JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = myUserDetailsService;
        this.jwtUtils = jwtUtils;
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
            throws Exception {
        logger.info("createAuthenticationToken()");
        logger.info("Username: " + authenticationRequest.getUsername());

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        String jwt = jwtUtils.generateToken(userDetails);
        String role = jwtUtils.extractRole(jwt);

        logger.info("Token: " + jwt);
        logger.info("Role: " + role);
        return ResponseEntity.ok(new AuthenticationResponse(jwt, role));
    }
}
