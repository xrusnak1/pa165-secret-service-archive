package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.dto.CountryDto;
import cz.muni.fi.pa165.facade.CountryFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author David Rusnak & Daniel Pecuch
 */
@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
@RequestMapping(value = "/country")

public class CountryController {

    final static Logger logger = LoggerFactory.getLogger(CountryController.class);

    CountryFacade countryFacade;

    @Autowired
    public CountryController(CountryFacade countryFacade) {
        this.countryFacade = countryFacade;
    }

    @GetMapping(path = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CountryDto> getCountries() {
        logger.info("getAllCountries()");
        return countryFacade.findAll();
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CountryDto getCountry(@PathVariable Long id) {
        logger.info("getCountry()");
        logger.info(id.toString());
        return countryFacade.find(id);
    }

    @PutMapping(path = "/{id}")
    public CountryDto updateCountry(@Valid @RequestBody CountryDto countryDto) {
        logger.info("updateCountry()");
        return countryFacade.update(countryDto);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteCountry(@PathVariable Long id) {
        logger.info("deleteCountry()");
        countryFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "/create")
    public void createCountry(@Valid @RequestBody CountryDto countryDto) {
        logger.info("createCountry()");
        countryFacade.create(countryDto);
    }
}
