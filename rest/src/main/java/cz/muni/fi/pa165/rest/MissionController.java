package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.MissionDetailsDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.facade.MissionFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author Jakub Hanko
 */
@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
@RequestMapping(value = "/mission")

public class MissionController {

    final static Logger logger = LoggerFactory.getLogger(MissionController.class);

    MissionFacade missionFacade;

    @Autowired
    public MissionController(MissionFacade missionFacade) {
        this.missionFacade = missionFacade;
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<MissionDto> getAllMissions() {
        logger.info("getAllMissions()");
        return missionFacade.findAll();
    }

    @GetMapping(path = "/{id}/suitableagents",produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AgentDto> getSuitableAgents(@PathVariable Long id) {
        logger.info("getSuitableAgents("+id.toString()+")");
        return missionFacade.findAgentsSuitableForMission(id);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public MissionDto getMission(@PathVariable Long id) {
        logger.info("getMission()");
        logger.info(id.toString());
        return missionFacade.find(id);
    }
    @GetMapping(path = "/details/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public MissionDetailsDto getMissionDetails(@PathVariable Long id) {
        logger.info("getMissionDetails("+id.toString()+")");
        return missionFacade.find(id, MissionDetailsDto.class);
    }

    @PutMapping(path = "/update/{id}")
    public MissionDto updateMission(@Valid @RequestBody MissionDto missionDto) {
        logger.info("updateMission()");
        return missionFacade.update(missionDto);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteMission(@PathVariable Long id) {
        logger.info("deleteMission()");
        missionFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "/create")
    public void createMission(@Valid @RequestBody MissionDto missionDto) {
        logger.info("createMission()");
        missionFacade.create(missionDto);
    }
}