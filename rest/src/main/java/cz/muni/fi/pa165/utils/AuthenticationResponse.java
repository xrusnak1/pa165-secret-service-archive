package cz.muni.fi.pa165.utils;

/**
 * @author David Myska
 */
public class AuthenticationResponse {

    private final String token;
    private final String role;

    public AuthenticationResponse(String token, String role) {
        this.token = token;
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public String getRole() {
        return role;
    }
}
