package cz.muni.fi.pa165;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import cz.muni.fi.pa165.persistence.PersistenceConfig;
import cz.muni.fi.pa165.service.BeanMappingServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * @author David Rusnak
 */
@Configuration
@Import(PersistenceConfig.class)
@ComponentScan(basePackageClasses = {BeanMappingServiceImpl.class})
public class ServiceConfiguration {

    @Bean
    public Mapper dozerBean() {
        Mapper dozerBean = DozerBeanMapperBuilder.buildDefault();
        return dozerBean;
    }

    @Bean
    public PasswordEncoder passEncoder()
    {
        return new BCryptPasswordEncoder();
    }

}
