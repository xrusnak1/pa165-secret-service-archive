package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.LoginCredentials;
import cz.muni.fi.pa165.persistence.entity.Mission;
import cz.muni.fi.pa165.service.AgentService;
import cz.muni.fi.pa165.service.AuthenticationService;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.MissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;


/**
 * @author David Myska
 */
@Component
@Transactional
public class AgentFacadeImpl implements AgentFacade {

    private AgentService agentService;

    private MissionService missionService;

    private AuthenticationService authenticationService;

    private BeanMappingService beanMappingService;

    @Autowired
    public AgentFacadeImpl(AgentService agentService, MissionService missionService,
                           AuthenticationService authenticationService, BeanMappingService beanMappingService) {
        this.agentService = agentService;
        this.missionService = missionService;
        this.authenticationService = authenticationService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public void create(AgentDto agentDto) {
        Agent agent = beanMappingService.map(agentDto, Agent.class);
        LoginCredentials loginCredentials = new LoginCredentials();
        loginCredentials.setUsername(agent.getCodename());
        loginCredentials.setPassword(agent.getCodename());
        loginCredentials.setRole("user");
        authenticationService.create(loginCredentials);
        agent.setLoginCredentials(authenticationService.findByName(agent.getCodename()));
        agentService.create(agent);
    }

    @Override
    public AgentDto find(Long id) {
        Agent agent = agentService.find(id);
        return beanMappingService.map(agent, AgentDto.class);
    }

    @Override
    public AgentDto update(AgentDto agentDto) {
        Agent agent = beanMappingService.map(agentDto, Agent.class);
        Agent updatedAgent = agentService.update(agent);
        return beanMappingService.map(updatedAgent, AgentDto.class);
    }

    @Override
    public void delete(Long id) {
        Agent agent = agentService.find(id);
        authenticationService.delete(agent.getLoginCredentials().getId());
        agentService.delete(id);
    }

    @Override
    public List<MissionDto> findMissionsSuitableForAgent(Long agentId) {
        Agent agent = agentService.find(agentId);
        List<Mission> missions = missionService.findMissionsSuitableForAgent(agent);
        return beanMappingService.map(missions, MissionDto.class);
    }

    @Override
    public Collection<AgentDto> findAll() {
        var agents = agentService.findAll();
        return beanMappingService.map(agents, AgentDto.class);
    }

    @Override
    public AgentDto findByName(String name) {
        return beanMappingService.map(agentService.findByName(name), AgentDto.class);
    }

    @Override
    public <Dto> Dto findByName(String name, Class<Dto> outputType) {
        return beanMappingService.map(agentService.findByName(name), outputType);
    }
}
