package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AssignmentCreateDto;
import cz.muni.fi.pa165.dto.AssignmentDto;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.service.AgentService;
import cz.muni.fi.pa165.service.AssignmentService;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.MissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * @author Andrea Fickova
 */
@Component
@Transactional
public class AssignmentFacadeImpl implements AssignmentFacade {

    AssignmentService assignmentService;

    AgentService agentService;

    MissionService missionService;

    BeanMappingService beanMappingService;

    @Autowired
    public AssignmentFacadeImpl(AssignmentService assignmentService, AgentService agentService,
                                MissionService missionService, BeanMappingService beanMappingService) {
        this.assignmentService = assignmentService;
        this.agentService = agentService;
        this.missionService = missionService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public void create(AssignmentDto assignmentDto) {
        Assignment assignment = beanMappingService.map(assignmentDto, Assignment.class);
        assignmentService.create(assignment);
    }

    @Override
    public AssignmentDto find(Long id) {
        Assignment assignment = assignmentService.find(id);
        return beanMappingService.map(assignment, AssignmentDto.class);
    }

    @Override
    public AssignmentDto update(AssignmentDto assignmentDto) {
        Assignment assignment = beanMappingService.map(assignmentDto, Assignment.class);
        Assignment updatedAssignment = assignmentService.update(assignment);
        return beanMappingService.map(updatedAssignment, AssignmentDto.class);
    }

    @Override
    public void delete(Long id) {
        assignmentService.delete(id);
    }

    @Override
    public Collection<AssignmentDto> findAll() {
        var agents = assignmentService.findAll();
        return beanMappingService.map(agents, AssignmentDto.class);
    }

    @Override
    public void createAssignment(AssignmentCreateDto dto) {
        var agent = agentService.find(dto.getAgentId());
        var mission = missionService.find(dto.getMissionId());
        Assignment assignment = beanMappingService.map(dto, Assignment.class);
        assignment.setAgent(agent);
        assignment.setMission(mission);
        assignmentService.create(assignment);
    }

}
