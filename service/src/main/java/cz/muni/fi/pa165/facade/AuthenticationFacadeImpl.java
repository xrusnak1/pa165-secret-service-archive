package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.LoginCredentialsDto;
import cz.muni.fi.pa165.persistence.entity.LoginCredentials;
import cz.muni.fi.pa165.service.AuthenticationService;
import cz.muni.fi.pa165.service.BeanMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
public class AuthenticationFacadeImpl implements AuthenticationFacade {

    BeanMappingService beanMappingService;

    AuthenticationService authenticationService;

    @Autowired
    public AuthenticationFacadeImpl(BeanMappingService beanMappingService,
                                    AuthenticationService authenticationService) {
        this.beanMappingService = beanMappingService;
        this.authenticationService = authenticationService;
    }

    @Override
    public LoginCredentialsDto getUserCredentials(String username) {
        var credentials = authenticationService.findByName(username);
        return beanMappingService.map(credentials, LoginCredentialsDto.class);
    }

    @Override
    public void create(LoginCredentialsDto dto) {
        LoginCredentials loginCredentials = beanMappingService.map(dto, LoginCredentials.class);
        authenticationService.create(loginCredentials);
    }

    @Override
    public LoginCredentialsDto find(Long id) {
        var credentials = authenticationService.find(id);
        return beanMappingService.map(credentials, LoginCredentialsDto.class);
    }

    @Override
    public LoginCredentialsDto update(LoginCredentialsDto dto) {
        LoginCredentials credentials = beanMappingService.map(dto, LoginCredentials.class);
        LoginCredentials updatedCredentials = authenticationService.update(credentials);
        return beanMappingService.map(updatedCredentials, LoginCredentialsDto.class);
    }

    @Override
    public void delete(Long id) {
        authenticationService.delete(id);
    }
}
