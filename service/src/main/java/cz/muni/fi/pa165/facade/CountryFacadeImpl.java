package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.CountryDto;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;


/**
 * @author David Rusnak
 */
@Component
@Transactional
public class CountryFacadeImpl implements CountryFacade {

    private CountryService countryService;

    private BeanMappingService beanMappingService;

    @Autowired
    public CountryFacadeImpl(CountryService countryService, BeanMappingService beanMappingService) {
        this.countryService = countryService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public void create(CountryDto countryDto) {
        Country country = beanMappingService.map(countryDto, Country.class);
        countryService.create(country);
    }

    @Override
    public CountryDto find(Long id) {
        Country country = countryService.find(id);
        return beanMappingService.map(country, CountryDto.class);
    }

    @Override
    public CountryDto update(CountryDto countryDto) {
        Country country = beanMappingService.map(countryDto, Country.class);
        Country updatedCountry = countryService.update(country);
        return beanMappingService.map(updatedCountry, CountryDto.class);
    }

    @Override
    public void delete(Long id) {
        countryService.delete(id);
    }

    @Override
    public Collection<CountryDto> findAll() {
        return countryService.findAll().stream().map(c -> beanMappingService.map(c, CountryDto.class)).collect(Collectors.toList());
    }
}
