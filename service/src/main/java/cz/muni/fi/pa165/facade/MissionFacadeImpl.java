package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Mission;
import cz.muni.fi.pa165.service.AgentService;
import cz.muni.fi.pa165.service.AssignmentService;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.MissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Jakub Hanko
 */
@Component
@Transactional
public class MissionFacadeImpl implements MissionFacade {

    @Autowired
    MissionService missionService;

    @Autowired
    AgentService agentService;

    @Autowired
    AssignmentService assignmentService;

    @Autowired
    BeanMappingService beanMappingService;

    @Autowired
    public MissionFacadeImpl(MissionService missionService, AgentService agentService,
                             AssignmentService assignmentService, BeanMappingService beanMappingService) {
        this.missionService = missionService;
        this.agentService = agentService;
        this.assignmentService = assignmentService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public void create(MissionDto missionDto) {
        Mission mission = beanMappingService.map(missionDto, Mission.class);
        missionService.create(mission);
    }

    @Override
    public MissionDto find(Long id) {
        Mission mission = missionService.find(id);
        return (mission == null) ? null : beanMappingService.map(mission, MissionDto.class);
    }

    @Override
    public MissionDto update(MissionDto missionDto) {
        Mission mission = beanMappingService.map(missionDto, Mission.class);
        Mission updatedMission = missionService.update(mission);
        return beanMappingService.map(updatedMission, MissionDto.class);
    }

    @Override
    public void delete(Long id) {
        missionService.delete(id);
    }

    @Override
    public void assignAgent(Long missionId, Long agentId, LocalDate start, LocalDate end) {
        Mission mission = missionService.find(missionId);
        Agent agent = agentService.find(agentId);
        Assignment assignment = new Assignment();
        assignment.setAgent(agent);
        assignment.setMission(mission);
        assignment.setStart(start);
        assignment.setFinish(end);

        assignmentService.create(assignment);
    }

    @Override
    public List<AgentDto> findAgentsSuitableForMission(Long missionId) {
        Mission mission = missionService.find(missionId);
        List<Agent> agents = agentService.findAgentsSuitableForMission(mission);
        return (agents == null) ? null : beanMappingService.map(agents, AgentDto.class);
    }

    @Override
    public List<MissionDto> findAll() {
        List<Mission> missions = missionService.listAllMissions();
        return (missions == null) ? null : beanMappingService.map(missions, MissionDto.class);
    }

    @Override
    public <Dto> Dto find(Long id, Class<Dto> outputType) {
        return beanMappingService.map(missionService.find(id), outputType);
    }
}
