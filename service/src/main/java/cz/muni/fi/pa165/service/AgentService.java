package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Mission;

import java.util.List;

/**
 * @author David Myska
 */
public interface AgentService extends BaseCrudService<Agent> {

    /**
     * Lists all the agents
     * @return List of all agents
     */
    List<Agent> findAll();

    /**
     * Finds an agent by his codename
     * @param codename Codename of the agent to be found
     * @return Agent with specified codename
     */
    Agent findByName(String codename);

    /**
     * Finds suitable agents for the specified mission
     * @param mission Mission for which you want to find the suitable agents
     * @return List of suitable agents for the specified mission
     */
    List<Agent> findAgentsSuitableForMission(Mission mission);
}
