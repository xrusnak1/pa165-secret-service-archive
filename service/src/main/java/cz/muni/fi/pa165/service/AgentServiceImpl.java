package cz.muni.fi.pa165.service;
import com.google.common.collect.Sets;
import cz.muni.fi.pa165.persistence.dao.AgentDao;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jakub Hanko
 */
@Service
public class AgentServiceImpl implements AgentService {

    private AgentDao agentDao;

    @Autowired
    public AgentServiceImpl(AgentDao agentDao) {
        this.agentDao = agentDao;
    }

    @Override
    public void create(Agent agent) {
        agentDao.create(agent);
    }

    @Override
    public List<Agent> findAll() {
        return agentDao.findAll();
    }

    @Override
    public Agent find(Long id) {
        return agentDao.find(id);
    }

    @Override
    public Agent update(Agent entity) {
        return agentDao.update(entity);
    }

    @Override
    public void delete(Long id) {
        agentDao.delete(id);
    }

    @Override
    public Agent findByName(String name) {
        return agentDao.findByName(name);
    }

    @Override
    public List<Agent> findAgentsSuitableForMission(Mission mission) {
        List<Agent> agents = agentDao.findAgentsAvailableInTime(mission.getStart(), mission.getDeadline());

        return agents.stream()
                .filter(a -> a.getSkills().containsAll(mission.getRequiredSkills())
                        && !Sets.intersection(a.getLanguageSkills(), mission.getCountry().getLanguages()).isEmpty())
                .collect(Collectors.toList());
    }
}
