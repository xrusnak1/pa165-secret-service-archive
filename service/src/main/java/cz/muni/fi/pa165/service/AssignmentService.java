package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.entity.Assignment;

import java.util.List;

/**
 * @author Andrea Fickova
 */

public interface AssignmentService extends BaseCrudService<Assignment> {
    List<Assignment> findAll();
}
