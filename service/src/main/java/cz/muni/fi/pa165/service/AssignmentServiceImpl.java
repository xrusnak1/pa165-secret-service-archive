package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.dao.AssignmentDao;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrea Fickova
 */

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private AssignmentDao assignmentDao;

    @Autowired
    public AssignmentServiceImpl(AssignmentDao assignmentDao) {
        this.assignmentDao = assignmentDao;
    }

    @Override
    public void create(Assignment entity) {
        assignmentDao.create(entity);
    }

    @Override
    public Assignment find(Long id) {
        return assignmentDao.find(id);
    }

    @Override
    public Assignment update(Assignment entity) {
        return assignmentDao.update(entity);
    }

    @Override
    public void delete(Long id) {
        assignmentDao.delete(id);
    }

    @Override
    public List<Assignment> findAll() {
        return assignmentDao.findAll();
    }
}
