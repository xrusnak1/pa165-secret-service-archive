package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.entity.LoginCredentials;

/**
 * @author David Myska
 */
public interface AuthenticationService extends BaseCrudService<LoginCredentials> {

    LoginCredentials findByName(String username);

}
