package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.dao.LoginCredentialsDao;
import cz.muni.fi.pa165.persistence.entity.LoginCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private LoginCredentialsDao loginCredentialsDao;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationServiceImpl(LoginCredentialsDao loginCredentialsDao, PasswordEncoder passwordEncoder) {
        this.loginCredentialsDao = loginCredentialsDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public LoginCredentials findByName(String username) {
        return loginCredentialsDao.findByUsername(username);
    }

    @Override
    public void create(LoginCredentials entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        loginCredentialsDao.create(entity);
    }

    @Override
    public LoginCredentials find(Long id) {
        return loginCredentialsDao.find(id);
    }

    @Override
    public LoginCredentials update(LoginCredentials entity) {
        return loginCredentialsDao.update(entity);
    }

    @Override
    public void delete(Long id) {
        loginCredentialsDao.delete(id);
    }
}
