package cz.muni.fi.pa165.service;

/**
 * @author David Myska
 */
public interface BaseCrudService<EntityType> {

    /**
     * Creates an entity and stores it in a database
     * @param entity Entity to be created
     */
    void create(EntityType entity);

    /**
     * Finds entity with given id
     * @param id of the entity to be found
     * @return found entity
     */
    EntityType find(Long id);

    /**
     * Updates given entity
     * @param entity entity to be updated
     * @return the managed instance that the state was merged to
     */
    EntityType update(EntityType entity);

    /**
     * Deletes entity with given id
     * @param id of entity to be deleted
     */
    void delete(Long id);
}
