package cz.muni.fi.pa165.service;

import com.github.dozermapper.core.Mapper;

import java.util.Collection;
import java.util.List;


/**
 * @author Jakub Hanko
 */

public interface BeanMappingService {

    <T> List<T> map(Collection<?> objects, Class<T> mapToClass);
    <T> T map(Object u, Class<T> mapToClass);
    Mapper getMapper();
}