package cz.muni.fi.pa165.service;

import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Jakub Hanko
 */
@Service
public class BeanMappingServiceImpl implements BeanMappingService {

    private Mapper mapper;

    @Autowired
    public BeanMappingServiceImpl(Mapper dozer) {
        this.mapper = dozer;
    }

    public <T> List<T> map(Collection<?> objects, Class<T> mapToClass) {
        List<T> mappedCollection = new ArrayList<>();
        for (Object object : objects) {
            mappedCollection.add(mapper.map(object, mapToClass));
        }
        return mappedCollection;
    }

    public <T> T map(Object u, Class<T> mapToClass) {
        return mapper.map(u,mapToClass);
    }

    public Mapper getMapper(){
        return mapper;
    }
}
