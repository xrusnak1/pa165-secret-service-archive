package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.entity.Country;

import java.util.Collection;

/**
 * @author David Rusnak
 */
public interface CountryService extends BaseCrudService<Country> {
    Collection<Country> findAll();
}
