package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.dao.CountryDao;
import cz.muni.fi.pa165.persistence.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author David Rusnak
 */
@Service
public class CountryServiceImpl implements CountryService {

    private CountryDao countryDao;

    @Autowired
    public CountryServiceImpl(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    @Override
    public void create(Country employee) {
        countryDao.create(employee);
    }

    @Override
    public Country find(Long id) {
        return countryDao.find(id);
    }

    public Country update(Country country) {
        return countryDao.update(country);
    }

    public void delete(Long id) {
        countryDao.delete(id);
    }

    public Collection<Country> findAll() {
        return countryDao.findAll();
    }
}
