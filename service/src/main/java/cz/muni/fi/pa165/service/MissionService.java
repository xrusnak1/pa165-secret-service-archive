package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Mission;

import java.util.List;

/**
 * @author Jakub Hanko
 */
public interface MissionService extends BaseCrudService<Mission> {

    /**
     * @param agent agent for which we shall find the suitable missions
     * @return missions suitable for agent
     */
    List<Mission> findMissionsSuitableForAgent(Agent agent);

    /**
     * @return List of all the existing missions.
     */
    List<Mission> listAllMissions();
}
