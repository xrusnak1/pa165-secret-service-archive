package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.persistence.dao.MissionDao;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Jakub Hanko
 */
@Service
public class MissionServiceImpl implements MissionService {

    private MissionDao missionDao;

    @Autowired
    public MissionServiceImpl(MissionDao missionDao) {
        this.missionDao = missionDao;
    }

    @Override
    public void create(Mission entity) {
        missionDao.create(entity);
    }

    @Override
    public Mission find(Long id) {
        return missionDao.find(id);
    }

    @Override
    public Mission update(Mission entity) {
        return missionDao.update(entity);
    }

    @Override
    public void delete(Long id) {
        missionDao.delete(id);
    }

    @Override
    public List<Mission> findMissionsSuitableForAgent(Agent agent) {
        // Out of future assignments find the one starting the earliest
        var assignment = agent.getAssignments().stream()
                .filter(a -> a.getStart().isAfter(LocalDate.now()))
                .min(Comparator.comparing(Assignment::getStart));

        // If agent has any future assignments restrict timeframe for suitable missions
        LocalDate date = LocalDate.now().plusYears(1);
        if (assignment.isPresent()) {
            date = assignment.get().getStart();
        }

        // Missions suitable by timeframe
        List<Mission> partiallySuitableMissions = missionDao.getFutureMissionsEndingBefore(date);

        return findSuitableMissions(agent, partiallySuitableMissions);
    }

    private List<Mission> findSuitableMissions(Agent agent, List<Mission> partiallySuitableMissions) {
        List<Mission> suitableMissions = new ArrayList<>();

        for (var mission : partiallySuitableMissions) {
            // Agent has to know at least 1 language of the target country
            boolean isSuitable = mission.getCountry().getLanguages().stream()
                    .anyMatch(lang -> agent.getLanguageSkills().contains(lang));

            // Agent has to have all the skills required by the mission
            isSuitable &= agent.getSkills().containsAll(mission.getRequiredSkills());

            if (isSuitable) {
                suitableMissions.add(mission);
            }
        }
        return suitableMissions;
    }

    @Override
    public List<Mission> listAllMissions() {
        return missionDao.findAll();
    }
}
