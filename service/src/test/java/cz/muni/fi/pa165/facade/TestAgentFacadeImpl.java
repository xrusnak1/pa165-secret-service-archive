package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.dto.*;
import cz.muni.fi.pa165.persistence.entity.*;
import cz.muni.fi.pa165.service.AgentService;
import cz.muni.fi.pa165.service.AuthenticationService;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.MissionService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author David Myska
 */
public class TestAgentFacadeImpl extends TestsBase {
    @Mock
    AgentService agentService;

    @Mock
    MissionService missionService;

    @Mock
    AuthenticationService authenticationService;

    @Spy
    @Autowired
    BeanMappingService beanMappingService;

    @InjectMocks
    AgentFacadeImpl agentFacade;

    Agent agent1;
    AgentDto agentDto1;

    Mission mission1;
    MissionDto missionDto1;

    Country country1;
    CountryDto countryDto1;

    Assignment assignment1;
    AssignmentDto assignmentDto1;

    LoginCredentials loginCredentials1;
    LoginCredentialsDto loginCredentialsDto1;

    @BeforeMethod
    public void setUp() {
        country1 = new Country();
        country1.setId(1L);
        country1.setName("Russia");
        country1.setLanguages(Set.of("ru"));
        country1.setLiteracy(0.87f);

        countryDto1 = new CountryDto();
        countryDto1.setId(1L);
        countryDto1.setName("Russia");
        countryDto1.setLanguages(Set.of("ru"));
        countryDto1.setLiteracy(0.87f);

        mission1 = new Mission();
        mission1.setId(1L);
        mission1.setName("Kill Osama");
        mission1.setStart(LocalDate.now());
        mission1.setDeadline(LocalDate.now().plusDays(1));
        mission1.setCountry(country1);

        missionDto1 = new MissionDto();
        missionDto1.setId(mission1.getId());
        missionDto1.setName(mission1.getName());
        missionDto1.setStart(mission1.getStart());
        missionDto1.setDeadline(mission1.getDeadline());
        missionDto1.setCountry(countryDto1);

        loginCredentials1 = new LoginCredentials();
        loginCredentials1.setId(1L);
        loginCredentials1.setUsername("Tester");
        loginCredentials1.setPassword("Tester");
        loginCredentials1.setRole("Tester");

        agent1 = new Agent();
        agent1.setId(1L);
        agent1.setCodename("Tester");
        agent1.setActiveSince(LocalDate.now());
        agent1.setDutyEnded(LocalDate.now());
        agent1.setLanguageSkills(Set.of("en"));
        agent1.setSkills(Set.of("piff paff"));
        agent1.setLoginCredentials(loginCredentials1);

        assignment1 = new Assignment(agent1, mission1, mission1.getStart(), mission1.getDeadline());
        agent1.setAssignments(Set.of(assignment1));

        loginCredentialsDto1 = new LoginCredentialsDto();
        loginCredentialsDto1.setId(1L);
        loginCredentialsDto1.setUsername("Tester");
        loginCredentialsDto1.setPassword("Tester");
        loginCredentialsDto1.setRole("Tester");

        agentDto1 = new AgentDto();
        agentDto1.setId(1L);
        agentDto1.setCodename(agent1.getCodename());
        agentDto1.setActiveSince(agent1.getActiveSince());
        agentDto1.setDutyEnded(agent1.getDutyEnded());
        agentDto1.setLanguageSkills(agent1.getLanguageSkills());
        agentDto1.setSkills(agent1.getSkills());
        agentDto1.setLoginCredentials(loginCredentialsDto1);

        assignmentDto1 = new AssignmentDto();
        assignmentDto1.setAgent(agentDto1);
        assignmentDto1.setMission(missionDto1);
        assignmentDto1.setStart(assignment1.getStart());
        assignmentDto1.setFinish(assignment1.getFinish());
        agentDto1.setAssignments(Set.of(assignmentDto1));

    }

    @Test
    public void testCreate() {
        agentFacade.create(agentDto1);
        verify(agentService, times(1)).create(agent1);
    }

    @Test
    public void testFind() {
        Mockito.when(agentService.find(1L)).thenReturn(agent1);
        Assert.assertEquals(agentFacade.find(1L), agentDto1);
    }

    @Test
    public void testUpdate() {
        Mockito.when(agentService.update(agent1)).thenReturn(agent1);
        agentFacade.update(agentDto1);
        verify(agentService, times(1)).update(agent1);
    }

    @Test
    public void testDelete() {
        Mockito.when(agentService.find(1L)).thenReturn(agent1);
        agentFacade.delete(1L);
        verify(agentService, times(1)).delete(1L);
        verify(authenticationService, times(1)).delete(1L);
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsEmptyList_whenNoSuitableMissionsFound() {
        Mockito.when(agentService.find(1L)).thenReturn(agent1);
        Mockito.when(missionService.findMissionsSuitableForAgent(agent1)).thenReturn(new ArrayList<Mission>());

        Assert.assertTrue(agentFacade.findMissionsSuitableForAgent(1L).isEmpty());
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsNonEmptyList_whenSuitableMissionsFound() {
        Mockito.when(agentService.find(1L)).thenReturn(agent1);
        var mockedList = new ArrayList<Mission>();
        mockedList.add(mission1);
        Mockito.when(missionService.findMissionsSuitableForAgent(agent1)).thenReturn(mockedList);

        var expectedList = new ArrayList<MissionDto>();
        expectedList.add(missionDto1);

        Assert.assertEquals(agentFacade.findMissionsSuitableForAgent(1L), expectedList);
    }

}
