package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.AssignmentDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Mission;
import cz.muni.fi.pa165.service.AssignmentService;
import cz.muni.fi.pa165.service.BeanMappingService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;

/**
 * @author Andrea Fickova, Daniel Pecuch
 * Daniel Pecuch: testAssignReportToAssignment
 * Andrea Fickova: The rest
 */
public class TestAssignmentFacadeImpl extends TestsBase {

    @Spy
    @Autowired
    BeanMappingService beanMappingService;

    @InjectMocks
    AssignmentFacadeImpl assignmentFacade;

    @Mock
    AssignmentService assignmentService;

    Assignment assignment1;

    AssignmentDto assignmentDto1;

    Mission mission1;

    MissionDto missionDto1;

    Agent agent1;

    AgentDto agentDto1;

    @BeforeMethod
    public void setUp() {

        agent1 = new Agent();
        mission1 = new Mission();
        assignment1 = new Assignment();
        assignment1.setId(1L);
        assignment1.setAgent(agent1);
        assignment1.setMission(mission1);
        assignment1.setStart(LocalDate.now());
        assignment1.setFinish(LocalDate.now().plusDays(1));

        agentDto1 = new AgentDto();
        missionDto1 = new MissionDto();
        assignmentDto1 = new AssignmentDto();
        assignmentDto1.setId(1L);
        assignmentDto1.setAgent(agentDto1);
        assignmentDto1.setMission(missionDto1);
        assignmentDto1.setStart(LocalDate.now());
        assignmentDto1.setFinish(LocalDate.now().plusDays(1));

        Mockito.when(beanMappingService.map(assignment1, AssignmentDto.class)).thenReturn(assignmentDto1);
        Mockito.when(beanMappingService.map(assignmentDto1, Assignment.class)).thenReturn(assignment1);
    }

    @Test
    public void testUpdate() {
        Mockito.when(assignmentService.update(assignment1)).thenReturn(assignment1);
        assignmentFacade.update(assignmentDto1);
        Mockito.verify(assignmentService, Mockito.times(1)).update(assignment1);
    }

    @Test
    public void testCreate() {
        assignmentFacade.create(assignmentDto1);
        Mockito.verify(assignmentService, Mockito.times(1)).create(assignment1);
    }

    @Test
    public void testFind() {
        Mockito.when(assignmentService.find(1L)).thenReturn(assignment1);
        Assert.assertEquals(assignmentFacade.find(1L), assignmentDto1);
    }

    @Test
    public void testDelete() {
        assignmentFacade.delete(1L);
        Mockito.verify(assignmentService, Mockito.times(1)).delete(assignment1.getId());
    }
}
