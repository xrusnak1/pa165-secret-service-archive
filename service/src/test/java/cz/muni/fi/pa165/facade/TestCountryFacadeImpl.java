package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.dto.CountryDto;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.CountryService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.mockito.Mockito.*;


/**
 * @author David Rusnak
 */
public class TestCountryFacadeImpl extends TestsBase {
    @Mock
    CountryService countryService;

    @Mock
    BeanMappingService beanMappingService;

    @InjectMocks
    CountryFacadeImpl countryFacade;

    Country country1;

    CountryDto countryDto1;

    @BeforeMethod
    public void setUp() {
        country1 = new Country();
        country1.setId(1L);
        country1.setName("Russia");
        country1.setLanguages(Set.of("ru"));
        country1.setLiteracy(0.87f);

        countryDto1 = new CountryDto();
        countryDto1.setId(1L);
        countryDto1.setName("Russia");
        countryDto1.setLanguages(Set.of("ru"));
        countryDto1.setLiteracy(0.87f);

        Mockito.when(beanMappingService.map(country1, CountryDto.class)).thenReturn(countryDto1);
        Mockito.when(beanMappingService.map(countryDto1, Country.class)).thenReturn(country1);
    }

    @Test
    public void testCreate() {
        countryFacade.create(countryDto1);
        verify(countryService, times(1)).create(country1);
    }

    @Test
    public void testFind() {
        Mockito.when(countryService.find(1L)).thenReturn(country1);
        Assert.assertEquals(countryFacade.find(1L), countryDto1);
    }

    @Test
    public void testUpdate() {
        when(countryService.update(country1)).thenReturn(country1);
        countryFacade.update(countryDto1);
        verify(countryService, times(1)).update(country1);
    }

    @Test
    public void testDelete() {
        countryFacade.delete(1L);
        verify(countryService, times(1)).delete(1L);
    }
}