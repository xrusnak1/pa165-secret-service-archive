package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.dto.AgentDto;
import cz.muni.fi.pa165.dto.CountryDto;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import cz.muni.fi.pa165.service.AgentService;
import cz.muni.fi.pa165.service.AssignmentService;
import cz.muni.fi.pa165.service.BeanMappingService;
import cz.muni.fi.pa165.service.MissionService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Hanko
 */
public class TestMissionFacadeImpl extends TestsBase {

    @Mock
    MissionService missionService;

    @Spy
    @Autowired
    BeanMappingService beanMappingService;

    @Mock
    AgentService agentService;

    @Mock
    AssignmentService assignmentService;

    @InjectMocks
    MissionFacadeImpl missionFacade;

    Mission mission1;
    MissionDto missionDto1;

    Agent agent1;
    AgentDto agentDto1;

    Country country1;
    CountryDto countryDto1;

    @BeforeMethod
    public void setUp() {
        country1 = new Country();
        country1.setId(1L);
        country1.setName("Russia");
        country1.setLiteracy(0.87f);

        countryDto1 = new CountryDto();
        countryDto1.setId(1L);
        countryDto1.setName("Russia");
        countryDto1.setLiteracy(0.87f);

        mission1 = new Mission();
        mission1.setId(1L);
        mission1.setName("Kill Osama");
        mission1.setStart(LocalDate.now());
        mission1.setDeadline(LocalDate.now().plusDays(1));
        mission1.setCountry(country1);

        missionDto1 = new MissionDto();
        missionDto1.setId(mission1.getId());
        missionDto1.setName(mission1.getName());
        missionDto1.setStart(mission1.getStart());
        missionDto1.setDeadline(mission1.getDeadline());
        missionDto1.setCountry(countryDto1);

        agent1 = new Agent();
        agent1.setId(1L);
        agent1.setCodename("Tester");
        agent1.setActiveSince(LocalDate.now());
        agent1.setDutyEnded(LocalDate.now());

        agentDto1 = new AgentDto();
        agentDto1.setId(1L);
        agentDto1.setCodename(agent1.getCodename());
        agentDto1.setActiveSince(agent1.getActiveSince());
        agentDto1.setDutyEnded(agent1.getDutyEnded());
    }

    @Test
    public void testUpdate() {
        when(missionService.update(mission1)).thenReturn(mission1);
        missionFacade.update(missionDto1);
        verify(missionService, times(1)).update(mission1);
    }

    @Test
    public void testFind() {
        when(missionService.find(1L)).thenReturn(mission1);
        Assert.assertEquals(missionFacade.find(1L), missionDto1);
    }

    @Test
    public void testCreate() {
        missionFacade.create(missionDto1);
        verify(missionService, times(1)).create(mission1);
    }

    @Test
    public void testDelete() {
        missionFacade.delete(1L);
        verify(missionService, times(1)).delete(1L);
    }

    @Captor
    ArgumentCaptor<Assignment> captor;

    @Test
    public void testAssignAgent() {
        LocalDate start = LocalDate.now();
        LocalDate end = LocalDate.now().plusDays(30);
        when(missionService.find(1L)).thenReturn(mission1);
        when(agentService.find(1L)).thenReturn(agent1);
        missionFacade.assignAgent(1L, 1L, start, end);

        verify(assignmentService).create(captor.capture());

        Assignment capturedAsg = captor.getValue();

        Assert.assertEquals(capturedAsg.getAgent(), agent1);
        Assert.assertEquals(capturedAsg.getMission(), mission1);
    }

    @Test
    public void testListAllMissions() {
        when(missionService.listAllMissions()).thenReturn(List.of(mission1));

        Assert.assertEquals(missionFacade.findAll(), List.of(missionDto1));
    }

    @Test
    public void testFindSuitableAgents_returnsEmptyList_whenNoSuitableAgentsFound() {
        when(missionService.find(1L)).thenReturn(mission1);
        when(agentService.findAgentsSuitableForMission(mission1)).thenReturn(new ArrayList<>());

        Assert.assertTrue(missionFacade.findAgentsSuitableForMission(1L).isEmpty());
    }

    @Test
    public void testFindSuitableAgents_returnsNonEmptyList_whenSuitableAgentsFound() {
        when(missionService.find(1L)).thenReturn(mission1);
        when(agentService.findAgentsSuitableForMission(mission1)).thenReturn(List.of(agent1));

        List<AgentDto> agents = missionFacade.findAgentsSuitableForMission(1L);

        Assert.assertEquals(agents, List.of(agentDto1));
    }

}
