package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.persistence.dao.AgentDao;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author David Myska
 */
public class TestAgentServiceImpl extends TestsBase {

    @Mock
    AgentDao agentDao;

    @InjectMocks
    AgentServiceImpl agentService;

    Mission mission1;

    Agent agent1;
    Agent agent2;

    Country country1;

    @BeforeMethod
    public void setUp() {
        agent1 = new Agent();
        agent1.setId(1L);
        agent1.setCodename("Tester");
        agent1.setActiveSince(LocalDate.of(1980, 1, 1));
        agent1.setDutyEnded(LocalDate.of(2000, 1, 1));
        agent1.setLanguageSkills(Set.of("en", "jp"));
        agent1.setSkills(Set.of("piff paff", "good heart"));

        agent2 = new Agent();
        agent2.setId(2L);
        agent2.setCodename("Tester's long lost brother");
        agent2.setActiveSince(LocalDate.of(1990, 1, 1));
        agent2.setDutyEnded(LocalDate.of(2000, 1, 1));
        agent2.setLanguageSkills(Set.of("en"));
        agent2.setSkills(Set.of("badassery", "good heart"));

        country1 = new Country();
        country1.setId(1L);
        country1.setName("Japan");
        country1.setLanguages(Set.of("jp"));

        mission1 = new Mission();
        mission1.setId(1L);
        mission1.setName("Kill Osama");
        mission1.setStart(LocalDate.of(1996, 1, 1));
        mission1.setDeadline(LocalDate.of(1997, 1, 1));
        mission1.setRequiredSkills(Set.of("piff paff"));
        mission1.setCountry(country1);
    }

    @Test
    public void testCreate() {
        agentService.create(agent1);

        Mockito.verify(agentDao, Mockito.times(1)).create(agent1);
    }

    @Test
    public void testFind() {
        Mockito.when(agentDao.find(1L)).thenReturn(agent1);
        Assert.assertEquals(agentService.find(1L), agent1);
    }

    @Test
    public void testUpdate() {
        Mockito.when(agentDao.update(agent1)).thenReturn(agent1);
        agentService.update(agent1);
        verify(agentDao, times(1)).update(agent1);
    }

    @Test
    public void testDelete() {
        agentService.delete(1L);
        verify(agentDao, times(1)).delete(1L);
    }

    @Test
    public void testFindAgentsSuitableForMission_oneSuitableAgent() {
        Mockito.when(agentDao.findAgentsAvailableInTime(any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(List.of(agent1));

        Assert.assertEquals(agentService.findAgentsSuitableForMission(mission1), List.of(agent1));
    }

    @Test
    public void testFindAgentsSuitableForMission_noSuitableAgents_langNok_skillsNok() {
        Mockito.when(agentDao.findAgentsAvailableInTime(any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(List.of(agent2));

        Assert.assertEquals(agentService.findAgentsSuitableForMission(mission1), List.of());
    }

    @Test
    public void testFindAgentsSuitableForMission_noSuitableAgents_skillsNok() {
        mission1.setRequiredSkills(Set.of("weapon stuff"));
        Mockito.when(agentDao.findAgentsAvailableInTime(any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(List.of(agent1, agent2));

        Assert.assertEquals(agentService.findAgentsSuitableForMission(mission1), List.of());
    }

    @Test
    public void testFindAgentsSuitableForMission_noSuitableAgents_langNok() {
        agent1.setLanguageSkills(Set.of("en"));
        Mockito.when(agentDao.findAgentsAvailableInTime(any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(List.of(agent1));

        Assert.assertEquals(agentService.findAgentsSuitableForMission(mission1), List.of());
    }

    @Test
    public void testFindAgentsSuitableForMission_multipleAgents() {
        mission1.setRequiredSkills(Set.of("good heart"));
        country1.setLanguages(Set.of("en", "jp"));

        Mockito.when(agentDao.findAgentsAvailableInTime(any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(List.of(agent1, agent2));

        Assert.assertEquals(agentService.findAgentsSuitableForMission(mission1), List.of(agent1, agent2));
    }
}
