package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.persistence.dao.AssignmentDao;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Andrea Fickova, Daniel Pecuch
 * Daniel Pecuch: testAssignReportToAssignment
 * Andrea Fickova: The rest
 */
public class TestAssignmentServiceImpl extends TestsBase {

    Assignment assignment1;

    @Mock
    AssignmentDao assignmentDao;

    @InjectMocks
    AssignmentServiceImpl assignmentService;

    @BeforeMethod
    public void setUp() {
        assignment1 = new Assignment();
        assignment1.setId(1L);
    }

    @Test
    public void testCreate() {
        assignmentService.create(assignment1);
        Mockito.verify(assignmentDao, Mockito.times(1)).create(assignment1);
    }

    @Test
    public void testFind() {
        Mockito.when(assignmentDao.find(1L)).thenReturn(assignment1);

        Assert.assertEquals(assignmentService.find(1L), assignment1);
    }

    @Test
    public void testUpdate() {
        Mockito.when(assignmentDao.update(assignment1)).thenReturn(assignment1);
        assignmentService.update(assignment1);
        verify(assignmentDao, times(1)).update(assignment1);
    }

    @Test
    public void testDelete() {
        assignmentService.delete(1L);

        verify(assignmentDao, times(1)).delete(1L);
    }
}
