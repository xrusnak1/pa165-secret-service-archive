package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.dto.MissionDto;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @author Jakub Hanko
 */
public class TestBeanMappingServiceImpl extends TestsBase {

    @Autowired
    BeanMappingService beanMappingService;

    Mission mission;
    Country country;

    @BeforeMethod
    public void setUp() {
        country = new Country();
        country.setId(1L);
        country.setName("Afghanistan");
        //country.setLanguages(Set.of(new Locale("us")));

        mission = new Mission();
        mission.setId(1L);
        mission.setName("Kill Osama");
        mission.setStart(LocalDate.now());
        mission.setDeadline(LocalDate.now().plusDays(1));
        mission.setCountry(country);

        mission.setResources(List.of("lots of ammo", "a bit of courage", "night vision goggles"));
        Assignment a = new Assignment(new Agent(), mission, LocalDate.now(), LocalDate.now().plusDays(1));
        mission.setAssignments(Set.of(a));
    }

    @Test
    public void testMap() {
        MissionDto missionDto = beanMappingService.map(mission, MissionDto.class);

        Assert.assertEquals(missionDto.getName(), mission.getName());
        Assert.assertEquals(missionDto.getResources(), mission.getResources());
        Assert.assertEquals(missionDto.getCountry().getName(), country.getName());
        //Assert.assertEquals(missionDto.getCountry().getLanguages(), country.getLanguages());
        //Assert.assertEquals(missionDto.getAssignments());
    }

    @Test
    public void testMapList() {
        List<MissionDto> missionDtos = beanMappingService.map(List.of(mission), MissionDto.class);

        Assert.assertEquals(missionDtos.size(), 1);
        Assert.assertEquals(missionDtos.get(0).getName(), mission.getName());
        Assert.assertEquals(missionDtos.get(0).getResources(), mission.getResources());
    }
}
