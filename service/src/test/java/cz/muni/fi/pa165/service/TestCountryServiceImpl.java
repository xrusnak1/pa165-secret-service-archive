package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.persistence.dao.CountryDao;
import cz.muni.fi.pa165.persistence.entity.Country;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.mockito.Mockito.*;

/**
 * @author David Rusnak
 */
public class TestCountryServiceImpl extends TestsBase {

    @Mock
    CountryDao countryDao;

    @InjectMocks
    CountryServiceImpl countryService;

    Country c1;

    @BeforeMethod
    public void setUp() {
        c1 = new Country();
        c1.setName("Russia");
        c1.setLiteracy(0.57f);
        c1.setLanguages(Set.of("russian"));
    }

    @Test
    public void testCreate() {
        countryService.create(c1);
        verify(countryDao, Mockito.times(1)).create(c1);
    }

    @Test
    public void testFind() {
        when(countryDao.find(1L)).thenReturn(c1);
        Assert.assertEquals(countryService.find(1L), c1);
    }

    @Test
    public void testUpdate() {
        c1.setLanguages(Set.of("russian", "kazach"));
        countryService.update(c1);
        verify(countryDao, times(1)).update(c1);
    }

    @Test
    public void testDelete() {
        countryService.delete(1L);
        verify(countryDao, times(1)).delete(1L);
    }
}