package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.TestsBase;
import cz.muni.fi.pa165.persistence.dao.MissionDao;
import cz.muni.fi.pa165.persistence.entity.Agent;
import cz.muni.fi.pa165.persistence.entity.Assignment;
import cz.muni.fi.pa165.persistence.entity.Country;
import cz.muni.fi.pa165.persistence.entity.Mission;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

/**
 * @author Jakub Hanko
 */
public class TestMissionServiceImpl extends TestsBase {

    Agent a1;
    Assignment assFirst;
    Assignment assSecond;
    Mission mSuitable1;
    Mission mSuitable2;
    Mission mNotSuitable1;
    Mission mNotSuitable2;
    Country cSuitable;
    Country cNotSuitable;
    LocalDate startSuitable1;
    LocalDate endSuitable1;
    LocalDate startSuitable2;
    LocalDate endSuitable2;
    LocalDate startNotSuitable1;
    LocalDate endNotSuitable1;
    List<String> skillsSuitable1;
    List<String> skillsSuitable2;
    List<String> skillsNotSuitable;

    Mission mission1;

    @Mock
    MissionDao missionDao;

    @InjectMocks
    MissionServiceImpl missionService;

    @BeforeMethod
    public void setUp() {
        mission1 = new Mission();
        mission1.setId(1L);
        mission1.setName("Kill Osama");
        mission1.setStart(LocalDate.now());

        a1 = new Agent();
        a1.setId(1L);
        a1.setCodename("Tester");
        a1.setActiveSince(LocalDate.now());
        a1.setDutyEnded(null);
        a1.setLanguageSkills(Set.of("en", "jp"));
        a1.setSkills(Set.of("w1", "w2", "w3"));

        skillsSuitable1 = Arrays.asList("w1", "w2");
        skillsSuitable2 = Arrays.asList("w1", "w3");
        skillsNotSuitable = Arrays.asList("w2", "w4");

        assFirst = new Assignment();
        assFirst.setStart(LocalDate.now().plusDays(10));
        assFirst.setFinish(LocalDate.now().plusDays(15));
        assSecond = new Assignment();
        assSecond.setStart(LocalDate.now().plusDays(16));
        assSecond.setFinish(LocalDate.now().plusDays(20));

        // doesn't require to be associated with mission in these tests
        // but may be needed elsewhere
        a1.addAssignment(assFirst);
        a1.addAssignment(assSecond);

        startSuitable1 = LocalDate.now().plusDays(2);
        endSuitable1 = startSuitable1.plusDays(2);
        startNotSuitable1 = LocalDate.now().minusDays(2);
        endNotSuitable1 = assFirst.getStart().plusDays(1);
        startSuitable2 = assFirst.getStart().minusDays(6);
        endSuitable2 = assFirst.getStart().minusDays(2);

        cSuitable = new Country("EN", Set.of("en"), 0.57f);
        cNotSuitable = new Country("CH", Set.of("zh"), 0.67f);

        mSuitable1 = new Mission();
        mSuitable1.setName("Suitable1");
        mSuitable1.setCountry(cSuitable);
        mSuitable1.setStart(startSuitable1);
        mSuitable1.setDeadline(endSuitable1);
        mSuitable1.setRequiredSkills(skillsSuitable1);

        mSuitable2 = new Mission();
        mSuitable2.setName("Suitable2");
        mSuitable2.setCountry(cSuitable);
        mSuitable2.setStart(startSuitable2);
        mSuitable2.setDeadline(endSuitable2);
        mSuitable2.setRequiredSkills(skillsSuitable2);

        // Nok Language
        mNotSuitable1 = new Mission();
        mNotSuitable1.setName("NotSuitable1");
        mNotSuitable1.setCountry(cNotSuitable);
        mNotSuitable1.setStart(startSuitable1);
        mNotSuitable1.setDeadline(endSuitable1);
        mNotSuitable1.setRequiredSkills(skillsSuitable1);

        // Nok skills
        mNotSuitable2 = new Mission();
        mNotSuitable2.setName("NotSuitable2");
        mNotSuitable2.setCountry(cSuitable);
        mNotSuitable2.setStart(startSuitable1);
        mNotSuitable2.setDeadline(endSuitable1);
        mNotSuitable2.setRequiredSkills(skillsNotSuitable);

    }

    @Test
    public void testCreate() {
        missionService.create(mission1);

        Mockito.verify(missionDao, Mockito.times(1)).create(mission1);
    }

    @Test
    public void testFind() {
        Mockito.when(missionDao.find(1L)).thenReturn(mission1);

        Assert.assertEquals(missionService.find(1L), mission1);
    }

    @Test
    public void testUpdate() {
        mission1.setDeadline(LocalDate.now().plusDays(1));
        missionService.update(mission1);

        verify(missionDao, times(1)).update(mission1);
    }

    @Test
    public void testDelete() {
        missionService.delete(1L);

        verify(missionDao, times(1)).delete(1L);
    }

    @Test
    public void testListAllMissions() {
        when(missionDao.findAll()).thenReturn(List.of(mission1));

        Assert.assertEquals(missionService.listAllMissions(), List.of(mission1));
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsOneMission_whenOneSuitableMissionIsPresent() {
        List<Mission> expected = new ArrayList<>();
        expected.add(mSuitable1);
        Mockito.when(missionDao.getFutureMissionsEndingBefore(assFirst.getStart())).thenReturn(expected);

        Assert.assertEquals(missionService.findMissionsSuitableForAgent(a1), expected);
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsEmptyList_whenTimeOkSkillsOkLanguageNok() {
        List<Mission> mocked = new ArrayList<>();
        mocked.add(mNotSuitable1);
        Mockito.when(missionDao.getFutureMissionsEndingBefore(assFirst.getStart())).thenReturn(mocked);

        Assert.assertTrue(missionService.findMissionsSuitableForAgent(a1).isEmpty());
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsEmptyList_whenTimeOkLanguageOkSkillsNok() {
        List<Mission> mocked = new ArrayList<>();
        mocked.add(mNotSuitable2);
        Mockito.when(missionDao.getFutureMissionsEndingBefore(assFirst.getStart())).thenReturn(mocked);

        Assert.assertTrue(missionService.findMissionsSuitableForAgent(a1).isEmpty());
    }

    @Test
    public void testFindMissionsSuitableForAgent_returnsSuitableMissions_whenSuitableAndNotSuitableMissionsArePresent() {
        List<Mission> mocked =
                Arrays.asList(mNotSuitable1, mSuitable1, mNotSuitable2, mSuitable2);
        Mockito.when(missionDao.getFutureMissionsEndingBefore(assFirst.getStart())).thenReturn(mocked);

        // Use sets because we don't care about order
        Set<Mission> expected = Set.of(mSuitable1, mSuitable2);
        Assert.assertEquals(Set.copyOf(missionService.findMissionsSuitableForAgent(a1)), expected);
    }
}
